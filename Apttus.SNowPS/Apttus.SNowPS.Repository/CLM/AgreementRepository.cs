﻿/*************************************************************
@Name: AgreementRepository.cs
@Author: Amey Khadatkar    
@CreateDate: 01-Nov-2017
@Description: This class contains the implementation for Agreement repository
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/

using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Apttus.SNowPS.Repository.CLM
{
    public class AgreementRepository
    {

        #region Private Fields
        /// <summary>
        /// Static Object of Agreement Repository
        /// </summary>
        private static readonly AgreementRepository agreementRepository = new AgreementRepository();
        #endregion

        #region Public Properties
        /// <summary>
        /// Property to store Access Token
        /// </summary>
        public string AccessToken { get; set; }
        #endregion

        #region Static Constructor
        static AgreementRepository()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Use to Get Single Instance of AgreementRepository
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static AgreementRepository Instance(string accessToken)
        {
            if (agreementRepository != null)
            {
                agreementRepository.AccessToken = accessToken;
            }
            return agreementRepository;
        }

        /// <summary>
        /// Assign the agreement to SalesOps Rep
        /// </summary>
        /// <param name="agreementId">agreement id</param>
        /// <param name="reason"></param>
        public void SubmitToSalesOps(string agreementId, string reason)
        {
            RequestModel request = new RequestModel()
            {
                select = new[] { AgreementObjectConstants.SalesOpsRep, AgreementObjectConstants.SubmitToSalesOpsReasons },
                filter = new List<FilterModel>
                {
                    new FilterModel
                    {
                        field = AgreementObjectConstants.Id,
                        @operator = "Equal",
                        value = new[] { agreementId }
                    }
                }
            };

            // Serializing the query
            var query = JsonConvert.SerializeObject(request);

            var reqConfig = new RequestConfigModel
            {
                accessToken = AccessToken,
                objectName = Constants.OBJ_AGREEMENT
            };

            // Get the agreement with column Sales Ops Rep
            var response = Utilities.GetSearch(query, reqConfig);

            var data = response.Content.ReadAsStringAsync().Result;

            dynamic jObj = JObject.Parse(data);
            string salesOpsRep = (string)jObj.SerializedResultEntities[0].ext_SalesOpsRep.Id;
            string submitToSalesOpsReasons = (string)jObj.SerializedResultEntities[0].ext_SubmitToSalesOpsReasons;
            List<Dictionary<string, object>> body = new List<Dictionary<string, object>>();

            Dictionary<string, object> agreementToUpdate = new Dictionary<string, object>
            {
                [AgreementObjectConstants.Id] = agreementId,
                [AgreementObjectConstants.SalesOpsRep] = new
                {
                    Id = salesOpsRep
                }
            };
            if (!string.IsNullOrWhiteSpace(submitToSalesOpsReasons))
            {
                agreementToUpdate[AgreementObjectConstants.SubmitToSalesOpsReasons] = submitToSalesOpsReasons + "," + reason;
            }
            else
            {
                agreementToUpdate[AgreementObjectConstants.SubmitToSalesOpsReasons] = reason;
            }
            body.Add(agreementToUpdate);

            // Update the owner field
            var updateResponse = Utilities.Update(body, new RequestConfigModel
            {
                accessToken = AccessToken,
                objectName = Constants.OBJ_AGREEMENT
            });

            var updateResult = updateResponse.Content.ReadAsStringAsync().Result;
        }

        /// <summary>
        /// Clones the specified agreement identifier.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <returns>New agreement id.</returns>
        /// <exception cref="Exception"></exception>
        public string Clone(Guid agreementId)
        {
            try
            {
                string url = string.Format(Constants.CLONEAGREEMENTAPIURL, agreementId);
                using (var client = new HttpClient(new HttpClientHandler()))
                {

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(AccessToken);

                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                    throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cleans the agreement clauses details.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <returns></returns>
        public string CleanAgreementClausesDetails(Guid agreementId)
        {
            var agreements = new List<dynamic>
            {
                new
                {
                    ext_AgreementClausesModified = string.Empty,
                    ext_AgreementClauseGroupModified = string.Empty,
                    ext_AgreementClausesIncluded = string.Empty,
                    ext_AgreementClauseGroupIncluded = string.Empty,
                    Id = agreementId
                }
            };
            var response = Utilities.Update(agreements, new RequestConfigModel
            {
                objectName = Constants.OBJ_AGREEMENT,
                resolvedID = agreementId.ToString(),
                accessToken = AccessToken
            });

            if (response?.Content != null)
            {
                return response.Content.ReadAsStringAsync().Result;
            }
            return string.Empty;
        }

        /// <summary>
        /// Populates the PDF templates.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <returns></returns>
        public string PopulatePdfTemplates(Guid agreementId)
        {
            try
            {
                /*Query expression*/
                var agreementLIQuery = new Query
                {
                    EntityName = AgreementLineItemConstants.ENTITYNAME,
                    Columns = AgreementLineItemConstants.FIELDS_PDFPOPULATION.Split(Constants.CHAR_COMMA).ToList(),
                    Criteria = new Expression
                    {
                        Conditions = new List<Condition>
                        {
                            new Condition /*Filter with agreementId*/
                            {
                                FieldName      = AgreementLineItemConstants.AGREEMENTID,
                                FilterOperator = FilterOperator.Equal,
                                Value          = agreementId
                            }
                        }
                    },
                    Joins = new List<Join>
                    {
                        new Join /* inner join with Agreement entity from clm_AgreementLineItem entity based on AgreementId */
                        {
                            EntityAlias   = AgreementObjectConstants.OBJ_ALIAS,
                            FromEntity    = AgreementLineItemConstants.ENTITYNAME,
                            FromAttribute = AgreementLineItemConstants.AGREEMENTID,
                            ToEntity      = AgreementObjectConstants.ENTITYNAME,
                            ToAttribute   = Constants.FIELD_ID,
                            JoinType      = JoinType.INNER,
                        },
                        new Join /* inner join with Quote entity from Agreement entity based on QuoteNumber on agreement. */
                        {
                            EntityAlias   = QuoteConstants.OBJ_ALIAS,
                            FromEntity    = AgreementObjectConstants.ENTITYNAME,
                            FromAttribute = AgreementObjectConstants.OBJ_ALIAS + Constants.CHAR_DOT + AgreementObjectConstants.EXT_QUOTENUMBER,
                            ToEntity      = QuoteConstants.ENTITYNAME,
                            ToAttribute   = QuoteConstants.EXT_QUOTENUMBER,
                            JoinType      = JoinType.INNER
                        },
                        new Join /* inner join with Product entity from clm_AgreementLineItem entity based on ProductId */
                        {
                            EntityAlias   = ProductConstants.OBJ_ALIAS,
                            FromEntity    = AgreementLineItemConstants.ENTITYNAME,
                            FromAttribute = AgreementLineItemConstants.PRODUCTID,
                            ToEntity      = ProductConstants.ENTITYNAME,
                            ToAttribute   = Constants.FIELD_ID,
                            JoinType      = JoinType.INNER
                        }
                    }
                };

                var response = Utilities.Search(agreementLIQuery.Serialize(), new RequestConfigModel
                {
                    accessToken = AccessToken,
                    searchType = SearchType.AQL,
                    objectName = AgreementLineItemConstants.ENTITYNAME
                });

                if (response?.Content != null)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                    var agreementLineItems = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);

                    var pdfTemplates = new List<string>();

                    if (agreementLineItems != null && agreementLineItems.Count > 0)
                    {
                        bool isServiceWatchProduct = true, isEventsOrEducationServiceProduct = true;
                        foreach (var agreementLineItem in agreementLineItems)
                        {
                            string quoteTypeFieldName = QuoteConstants.OBJ_ALIAS + Constants.CHAR_DOT + QuoteConstants.EXT_TYPE;
                            string quoteType = agreementLineItem.ContainsKey(quoteTypeFieldName) ? agreementLineItem[quoteTypeFieldName].ConvertToString() : string.Empty;

                            if (quoteType == QuoteConstants.EXT_TYPE_SERVICES || quoteType == QuoteConstants.EXT_TYPE_TRAINING)
                            {
                                pdfTemplates.Add(AgreementObjectConstants.EXT_PDFTEMPLATE_SERVICES);
                            }

                            var productId = agreementLineItem.ContainsKey(AgreementLineItemConstants.PRODUCTID) ? JObject.FromObject(agreementLineItem[AgreementLineItemConstants.PRODUCTID])?.ToObject<Dictionary<string, object>>() : null;

                            string productName;
                            var productFamily = productName = string.Empty;
                            if (productId != null)
                            {
                                string productNameFieldName = ProductConstants.OBJ_ALIAS + Constants.CHAR_DOT + Constants.FIELD_NAME;
                                productName = productId.ContainsKey(productNameFieldName) ? productId[productNameFieldName]?.ToString() : string.Empty;

                                string productFamilyFieldName = ProductConstants.OBJ_ALIAS + Constants.CHAR_DOT + ProductConstants.FAMILY;
                                productFamily = productId.ContainsKey(productFamilyFieldName) ? productId[productFamilyFieldName]?.ToString() : string.Empty;
                            }

                            int deltaQty = agreementLineItem.ContainsKey(AgreementLineItemConstants.EXT_DELTAQUANTITY) && int.TryParse(agreementLineItem[AgreementLineItemConstants.EXT_DELTAQUANTITY]?.ToString(), out deltaQty) ? deltaQty : 0;

                            int contractQty = agreementLineItem.ContainsKey(AgreementLineItemConstants.EXT_CONTRACTQUANTITY) && int.TryParse(agreementLineItem[AgreementLineItemConstants.EXT_CONTRACTQUANTITY]?.ToString(), out deltaQty) ? deltaQty : 0;

                            if (productName != null && ((deltaQty > 0 || contractQty > 0) && !productName.Trim().Contains(ProductConstants.NAME_SERVICEWATCH)))
                            {
                                isServiceWatchProduct = false;
                            }

                            if ((deltaQty > 0 || contractQty > 0) && !(productFamily == ProductConstants.FAMILY_EDUCATIONSERVICES || productFamily == ProductConstants.FAMILY_EVENTS))
                            {
                                isEventsOrEducationServiceProduct = false;
                            }

                        }

                        if (isServiceWatchProduct)
                        {
                            pdfTemplates.Add(AgreementObjectConstants.EXT_PDFTEMPLATE_SERVICEWATCH);
                        }

                        if (isEventsOrEducationServiceProduct)
                        {
                            pdfTemplates.Add(AgreementObjectConstants.EXT_PDFTEMPLATE_KNOWLEDGEEVENT);
                        }

                        if (pdfTemplates.Count <= 0)
                        {
                            pdfTemplates.Add(AgreementObjectConstants.EXT_PDFTEMPLATE_FULFILLERORDERFORM);
                        }

                        string pdfTemplatesString = string.Join(Constants.CHAR_COMMA.ToString(), pdfTemplates.Distinct().ToArray());

                        var agreements = new List<dynamic>
                        {
                            new
                            {
                                Id = agreementId,
                                ext_PDFTemplate = pdfTemplatesString
                            }
                        };
                        var updateResponse = Utilities.Update(agreements, new RequestConfigModel
                        {
                            objectName = AgreementObjectConstants.ENTITYNAME,
                            resolvedID = agreementId.ToString()
                        });

                        return updateResponse?.Content?.ReadAsStringAsync().Result;
                    }

                }
            }
            catch (Exception exc)
            {
                string error = exc.Message;

                if (exc.InnerException != null)
                {
                    error += Environment.NewLine + exc.InnerException.Message;
                }
                return error;
            }
            return Constants.MSG_SUCCESS;
        }

        /// <summary>
        /// Populates Aggregated AgreementFields based on QLI.
        /// </summary>
        /// <param name="agreementModel">Agreement Object Model</param>
        /// <param name="QLIfields"></param>
        /// <returns></returns>
        public string CalculatedAgreementFieldsFromQLI(AgreementObjectModel agreementModel, string QLIfields)
        {
            try
            {
                if (agreementModel.ext_quoteid == null)
                {
                    var agreementQuery = new Query
                    {
                        EntityName = AgreementObjectConstants.ENTITYNAME,
                        Columns = new List<string> { AgreementObjectConstants.EXT_QUOTEID },
                        Criteria = new Expression
                        {
                            Conditions =
                            {
                                new Condition
                                {
                                    FieldName = AgreementObjectConstants.Id,
                                    FilterOperator = FilterOperator.Equal,
                                    Value = agreementModel.Id
                                }
                            }
                        }
                    };

                    var agreementResponse = Utilities.GetSearch(agreementQuery.Serialize(), new RequestConfigModel { objectName = AgreementObjectConstants.ENTITYNAME, resolvedID = agreementModel.Id });
                    var responseString = agreementResponse.Content.ReadAsStringAsync().Result;
                    var agreements = new List<AgreementObjectModel>();
                    if (agreementResponse.IsSuccessStatusCode && !string.IsNullOrEmpty(responseString))
                    {
                        agreements = JObject.Parse(responseString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<AgreementObjectModel>>();
                    }
                    agreementModel.ext_quoteid = agreements[0].ext_quoteid;
                }

                //Query Expression to extract QuoteLineItems
                var quoteLineItemsQuery = new Query
                {
                    EntityName = QuoteLineItemConstants.ENTITYNAME,
                    Columns = QLIfields.Split(Constants.CHAR_COMMA).ToList(),
                    Criteria = new Expression
                    {
                        Conditions =
                        {
                            new Condition
                            {
                                FieldName = QuoteLineItemConstants.QUOTEID,
                                FilterOperator = FilterOperator.Equal,
                                Value = agreementModel.ext_quoteid
                            }
                        }
                    }
                };

                var response = Utilities.Search(quoteLineItemsQuery.Serialize(), new RequestConfigModel
                {
                    accessToken = AccessToken,
                    searchType = SearchType.AQL,
                    objectName = QuoteLineItemConstants.ENTITYNAME
                });

                if (response?.Content != null)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();
                    var quoteLineItems = JsonConvert.DeserializeObject<List<QuoteLineItemModel>>(responseString);
                    if (quoteLineItems != null && quoteLineItems.Count > 0)
                    {
                        var userTypes = new StringBuilder();
                        string release = string.Empty;
                        List<string> releaseEntries = new List<string>();
                        List<string> userTypeEntries = new List<string>();

                        quoteLineItems.ForEach(quoteLineItem =>
                        {
                            if (quoteLineItem.Quantity > 0)
                            {
                                releaseEntries.Add(quoteLineItem.ext_Release);
                                if (!userTypeEntries.Contains(quoteLineItem.ext_Type.Value))
                                    userTypeEntries.Add(quoteLineItem.ext_Type.Value);
                            }
                        });
                        releaseEntries = releaseEntries.OrderByDescending(r => r).ToList();
                        var dictContent = new Dictionary<string, object>
                        {
                            {AgreementObjectConstants.EXT_RELEASE,  releaseEntries[0]},
                            {AgreementObjectConstants.EXT_USERTYPE,  string.Join(",", userTypeEntries.ToArray())},
                        };

                        var updateResponse = Utilities.Update(new List<Dictionary<string, object>> { dictContent }, new RequestConfigModel { objectName = AgreementObjectConstants.ENTITYNAME, resolvedID = agreementModel.Id });

                        if (updateResponse != null)
                        {
                            return updateResponse.Content.ReadAsStringAsync().Result;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                string error = exc.Message;

                if (exc.InnerException != null)
                {
                    error += Environment.NewLine + exc.InnerException.Message;
                }
                return error;
            }
            return Constants.MSG_SUCCESS;
        }

        /// <summary>
        /// Returns the related Agreements based on the Quotes
        /// </summary>
        /// <param name="quote">Quote</param>
        /// <param name="selectFields"></param>
        /// <returns></returns>
        public List<Dictionary<string, Object>> GetQuoteRelatedAgreements(Dictionary<string, object> quote, List<string> selectFields)
        {
            var agreements = new List<Dictionary<string, Object>>();
            var query = new Query
            {
                EntityName = Constants.OBJ_AGREEMENT,
                Columns = selectFields,
                Criteria = new Expression
                {
                    Conditions =
                        {
                            new Condition
                            {
                                FieldName = AgreementObjectConstants.EXT_QUOTEID,
                                FilterOperator = FilterOperator.Equal,
                                Value = quote[Constants.FIELD_ID]
                            }
                        }
                }
            };
            var responseMessage = Utilities.Search(query.Serialize(), new RequestConfigModel { accessToken = AccessToken, objectName = Constants.OBJ_AGREEMENT });
            var responseString = responseMessage.Content.ReadAsStringAsync().Result;
            if (responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(responseString))
            {
                agreements = JObject.Parse(responseString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, Object>>>();
            }
            return agreements;
        }

        /// <summary>
        /// Sets the default values for the list of fields mentioned in product setting value.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <returns></returns>
        public string SetDefaultValues(Guid agreementId)
        {
            var fieldDetails = new List<Dictionary<string, object>>();
            var query = new Query
            {
                EntityName = ProductSettingValueConstants.ENTITYNAME,
                Columns = new List<string> { ProductSettingValueConstants.VALUE },
                Criteria = new Expression
                {
                    Conditions = new List<Condition>
                    {
                        new Condition
                        {
                            FieldName = Constants.FIELD_NAME,
                            FilterOperator = FilterOperator.Equal,
                            Value = ProductSettingValueConstants.VALUE_AGREEMENTFIELDSANDDEFAULTVALUES
                        }
                    }
                }
            };

            var response = Utilities.Search(query.Serialize(), new RequestConfigModel
            {
                accessToken = AccessToken,
                searchType = SearchType.AQL,
                objectName = ProductSettingValueConstants.ENTITYNAME
            });

            if (response?.Content != null)
            {
                var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                var productSettingValues = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);

                if (productSettingValues != null && productSettingValues.Count > 0 && productSettingValues[0]?[ProductSettingValueConstants.VALUE] != null)
                {
                    fieldDetails = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(productSettingValues[0][ProductSettingValueConstants.VALUE].ConvertToString());
                }
            }

            var fieldNames = new List<string>();

            foreach (var item in fieldDetails)
            {
                fieldNames.Add(item[ProductSettingValueConstants.FIELD].ToString());
            }

            query = new Query
            {
                EntityName = AgreementObjectConstants.ENTITYNAME,
                Columns = fieldNames,
                Criteria = new Expression
                {
                    Conditions = new List<Condition>
                    {
                        new Condition
                        {
                            FieldName = Constants.FIELD_ID,
                            FilterOperator = FilterOperator.Equal,
                            Value = agreementId
                        }
                    }
                }
            };

            response = Utilities.Search(query.Serialize(), new RequestConfigModel
            {
                accessToken = AccessToken,
                searchType = SearchType.AQL,
                objectName = AgreementObjectConstants.ENTITYNAME
            });

            if (response?.Content != null)
            {
                var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                var agreements = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);

                if (agreements != null && agreements.Count > 0)
                {
                    var agreement = agreements[0];
                    if (agreement != null)
                    {
                        var agreementToUpdate = new Dictionary<string, object> {{Constants.FIELD_ID, agreementId}};
                        foreach (var field in fieldNames)
                        {
                            if (agreement.ContainsKey(field) && (agreement[field] == null || agreement[field].ConvertToInteger() == 0 || agreement[field].ConvertToString() == string.Empty))
                            {
                                var value = fieldDetails.FirstOrDefault(fieldDetail => fieldDetail[ProductSettingValueConstants.FIELD]?.ConvertToString() == field)?[ProductSettingValueConstants.VALUE];
                                agreementToUpdate.Add(field, value);
                            }
                        }

                        var agreementsToUpdate = new List<dynamic> {agreementToUpdate};

                        var updateResponse = Utilities.Update(agreementsToUpdate, new RequestConfigModel
                        {
                            objectName = AgreementObjectConstants.ENTITYNAME,
                            resolvedID = agreement[Constants.FIELD_ID].ToString(),
                            accessToken = AccessToken
                        });

                        return updateResponse?.Content?.ReadAsStringAsync().Result;
                    }
                }
            }
            return string.Empty;
        }
        public bool CopyFromSalesMasterAgreement(Guid agreementId)
        {
            try
            {
                Dictionary<string, object> AgreementRecord = new Dictionary<string, object>();
                var AgreementFetchColumns = GetProductSettingValue(AgreementObjectConstants.SALESMASTERAGREEMENTFIELDSTOCOPY, AgreementObjectConstants.SALESMASTERAGREEMENTOBJ_ALIAS);
                if (AgreementFetchColumns != null && AgreementFetchColumns.Count > 0)
                {
                    var agreementSMQuery = new Query
                    {
                        EntityName = AgreementObjectConstants.ENTITYNAME,
                        Columns = AgreementFetchColumns,
                        Criteria = new Expression
                        {
                            Conditions = new List<Condition>
                        {
                            new Condition
                            {
                                FieldName      = AgreementObjectConstants.Id,
                                FilterOperator = FilterOperator.Equal,
                                Value          = agreementId
                            }
                        }
                        },
                        Joins = new List<Join>
                    {
                        new Join
                        {
                            EntityAlias   = AgreementObjectConstants.SALESMASTERAGREEMENTOBJ_ALIAS,
                            FromEntity    = AgreementObjectConstants.ENTITYNAME,
                            FromAttribute = AgreementObjectConstants.Id,
                            ToEntity      = AgreementObjectConstants.ENTITYNAME,
                            ToAttribute   = AgreementObjectConstants.MASTERCONTRACT,
                            JoinType      = JoinType.INNER,
                        }
                }
                    };
                    var response = Utilities.Search(agreementSMQuery.Serialize(), new RequestConfigModel
                    {
                        accessToken = AccessToken,
                        searchType = SearchType.AQL,
                        objectName = AgreementObjectConstants.ENTITYNAME
                    });

                    if (response?.Content != null)
                    {
                        var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();
                        var childAgreement = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);
                        var salesMasterAgreement = (childAgreement.Count > 0 && childAgreement[0].ContainsKey("SALESMASTERAGMNT") && childAgreement[0]["SALESMASTERAGMNT"] != null) ? JsonConvert.DeserializeObject<Dictionary<string, object>>(childAgreement[0]["SALESMASTERAGMNT"].ToString()) : null;

                        if (salesMasterAgreement != null)
                        {
                            List<Dictionary<string, object>> body = new List<Dictionary<string, object>>();

                            Dictionary<string, object> agreementToUpdate = new Dictionary<string, object>
                            {
                                [AgreementObjectConstants.Id] = agreementId
                            };
                            foreach (string field in AgreementFetchColumns)
                            {
                                string strColumnName = field.Replace(AgreementObjectConstants.SALESMASTERAGREEMENTOBJ_ALIAS + ".", "");
                                if (salesMasterAgreement.ContainsKey(strColumnName))
                                    agreementToUpdate[strColumnName] = salesMasterAgreement[strColumnName];
                            }
                            body.Add(agreementToUpdate);

                            var updateResponse = Utilities.Update(body, new RequestConfigModel
                            {
                                accessToken = AccessToken,
                                objectName = Constants.OBJ_AGREEMENT
                            });

                            var updateResult = updateResponse.Content.ReadAsStringAsync().Result;

                            return true;
                        }

                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private List<string> GetProductSettingValue(string key, string alias)
        {
            try
            {
                string value = "";
                var productSettingQuery = new Query
                {
                    EntityName = ProductSettingConstants.ENTITYNAME,
                    Columns = ProductSettingConstants.FIELDS_PRODUCTSETTING.Split(Constants.CHAR_COMMA).ToList(),
                    Criteria = new Expression
                    {
                        Conditions = new List<Condition>
                        {
                            new Condition
                            {
                                FieldName      = ProductSettingConstants.NAME,
                                FilterOperator = FilterOperator.Equal,
                                Value          = key
                            }
                        }
                    }
                };
                var response = Utilities.Search(productSettingQuery.Serialize(), new RequestConfigModel
                {
                    accessToken = AccessToken,
                    searchType = SearchType.AQL,
                    objectName = ProductSettingConstants.ENTITYNAME
                });

                if (response?.Content != null)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                    var salesMasterAgreement = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);

                    var pdfTemplates = new List<string>();

                    if (salesMasterAgreement != null && salesMasterAgreement.Count > 0)
                    {
                        var sm = salesMasterAgreement.First();
                        value = sm[ProductSettingConstants.VALUE].ToString();
                    }

                }
                List<string> columns = new List<string>();
                var fields = value.Split(',');
                foreach (string strField in fields)
                {
                    columns.Add(alias + "." + strField);
                }
                return columns;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion
    }
}
