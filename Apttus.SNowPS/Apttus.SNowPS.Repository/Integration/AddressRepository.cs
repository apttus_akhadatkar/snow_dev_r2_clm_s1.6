﻿/****************************************************************************************
@Name: AddressRepository.cs
@Author: Varun Shah
@CreateDate: 6 Oct 2017
@Description: Address Related Computations
@UsedBy: This will be used by AddressController.cs

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Respository
{
    public class AddressRepository
    {
        #region Properties
        string addressSyncUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        private string muleXSourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];
        #endregion

        #region Methods
        /// <summary>
        /// Resolves external ids and upserts contacts using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertAddress(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                var errors = new List<Model.ErrorInfo>();
                var responseAddress = new HttpResponseMessage();
                var responseAddressByExtID = new HttpResponseMessage();
                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                //Resolve Account MDM Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_EXT_ACCOUNTMDMID;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_ACCOUNTID;

                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Remove unresolved requests
                Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_EXTERNALID);

                if (dictContent != null && dictContent.Count > 0)
                {
                    //Create Dictionary content based on ID 
                    var dicAddress = new List<Dictionary<string, object>>();
                    foreach (var address in dictContent.ToList())
                    {
                        if (address.ContainsKey(Constants.FIELD_ID) 
                            && !string.IsNullOrEmpty(Convert.ToString(address[Constants.FIELD_ID])))
                        {
                            dicAddress.Add(address);
                            dictContent.Remove(address);
                        }
                    }

                    //Upsert Address By ExternalID
                    if (dictContent != null && dictContent.Count > 0)
                    {                       
                        reqConfig.objectName = Constants.OBJ_ACCOUNTLOCATION;
                        reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                        responseAddressByExtID = Utilities.Upsert(dictContent, reqConfig);
                    } 

                    //Upsert Address By ID
                    if (dicAddress != null && dicAddress.Count > 0)
                    {
                        reqConfig.objectName = Constants.OBJ_ACCOUNTLOCATION;
                        reqConfig.UpsertKey = Constants.FIELD_ID;
                        var responseAddressByID = Utilities.Upsert(dicAddress, reqConfig);
                        if(responseAddressByExtID != null && responseAddressByExtID.Content != null)
                            responseAddressByExtID = Utilities.CreateResponse(responseAddressByExtID, responseAddressByID);
                        else
                            responseAddressByExtID = Utilities.CreateResponse(responseAddressByID);
                    }

                    return Utilities.CreateResponse(responseAddressByExtID, errors);
                }
                else
                {
                    var bulkResponseModel = new BulkOperationResponseModel();
                    var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                    responseAddress.Content = Utilities.CreateJsonContent(jsonResponse);

                    //Create Response by adding custom errors
                    return Utilities.CreateResponse(responseAddress, errors);
                }
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Get Apttus Address Data By ContactId
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage GetApttusDataByAddressId(List<Dictionary<string, object>> dictContent)
        {
            try
            {
                string addressSyncUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];

                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var requestConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNTLOCATION, Constants.FIELD_ID, Constants.FIELD_ID);

                return Utilities.Search(dictContent, requestConfig);
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Use for Address Sync
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage SendAddressDataToMule(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                string objResponse = string.Empty;
                string resContactString = string.Empty;
                #region GetApttusAddressData

                string addressSyncUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];

                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var dictAdd = dictContent.FirstOrDefault().ToDictionary(k => k.Key, k => Convert.ToString(k.Value));

                string eventType = string.Empty;

                if (dictAdd.ContainsKey(Constants.FIELD_EVENTTYPE))
                {
                    eventType = dictAdd[Constants.FIELD_EVENTTYPE];
                    dictContent.FirstOrDefault().Remove(Constants.FIELD_EVENTTYPE);
                }

                var requestConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNTLOCATION, Constants.FIELD_ID, Constants.FIELD_ID);

                var resMessageAddress = Utilities.Search(dictContent, requestConfig);

                #endregion
                if (resMessageAddress.IsSuccessStatusCode)
                {
                    string accountId = string.Empty;
                    string ExternalId = string.Empty;
                    AddressModel responseAddress = new AddressModel();

                    if (eventType != Constants.FIELD_ADDRESSSENDTOSAPEVENTTYPE)
                    {
                        resContactString = resMessageAddress.Content.ReadAsStringAsync().Result;
                        var add = JObject.Parse(resContactString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>().First();
                        object dt;
                        add.TryGetValue("ModifiedOn", out dt);
                        string ModifiedOn = dt != null ? Convert.ToDateTime(dt).ToString("yyyy-MM-dd'T'HH:mm:ss.fff") : null;
                        var dictAddresses = new Dictionary<string, string>();
                        dictAddresses = add.ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                        dictAddresses[Constants.FIELD_ACCOUNTID] = dictAddresses[Constants.FIELD_ACCOUNTID] != null ? JsonConvert.DeserializeObject<AccountModel>(dictAddresses[Constants.FIELD_ACCOUNTID]).srcPartyId : null;
                        accountId = dictAddresses[Constants.FIELD_ACCOUNTID];
                        ExternalId = dictAddresses[Constants.FIELD_EXTERNALID];
                        string str = new JavaScriptSerializer().Serialize(dictAddresses);
                        responseAddress = JsonConvert.DeserializeObject<AddressModel>(str);
                        responseAddress.lastUpdatedDateTime = ModifiedOn;
                    }
                    else
                    {
                        var reqConfig = new RequestConfigModel();
                        reqConfig.accessToken = accessToken;
                        reqConfig.select = new string[] { Constants.FIELD_ACCOUNTID };
                        reqConfig.objectName = Constants.OBJ_AGREEMENT;
                        reqConfig.apttusFilterField = Constants.FIELD_ID;
                        reqConfig.externalFilterField = Constants.FIELD_ID;
                        var searchresponse = Utilities.Search(dictContent, reqConfig);
                        var agreementAcc = JObject.Parse(searchresponse.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>().FirstOrDefault();
                        if (agreementAcc.ContainsKey(Constants.FIELD_ACCOUNTID))
                        {
                            accountId = JsonConvert.DeserializeObject<LookUp>(Convert.ToString(agreementAcc[Constants.FIELD_ACCOUNTID])).Id;
                        }
                    }

                    if (!string.IsNullOrEmpty(accountId))
                    {
                        #region GetApttusAccountData
                        var reConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNT, Constants.FIELD_ID, Constants.FIELD_ID);

                        List<Dictionary<string, object>> contentAccount = new List<Dictionary<string, object>>();
                        var dic = new Dictionary<string, object>();
                        dic.Add(Constants.FIELD_ID, (object)accountId);
                        contentAccount.Add(dic);
                        contentAccount = Utilities.GetCaseIgnoreDictContent(contentAccount);

                        var resMessageAccountAddress = Utilities.Search(contentAccount, reConfig);
                        #endregion
                        string resAccountAddressString = string.Empty;

                        if (resMessageAccountAddress.IsSuccessStatusCode)
                        {
                            resAccountAddressString = resMessageAccountAddress.Content.ReadAsStringAsync().Result;
                            var resultAccount = JObject.Parse(resAccountAddressString);

                            var acc = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(resultAccount[Constants.NODE_SERIALIZEDRESULTENTITIES].ToString());
                            object dtModified;
                            acc.FirstOrDefault().TryGetValue("ModifiedOn", out dtModified);
                            string accModifiedOn = dtModified != null ? Convert.ToDateTime(dtModified).ToString("yyyy-MM-dd'T'HH:mm:ss.fff") : null;
                            var dictAccount = acc.FirstOrDefault();
                            var dictAccounts = new Dictionary<string, string>();
                            if (dictAccount != null && dictAccount.Count > 0)
                            {
                                #region SendAddressDataToMule
                                dictAccounts = dictAccount.ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                                //dictAccounts[Constants.FIELD_TYPE] = dictAccounts[Constants.FIELD_TYPE] != null ? JsonConvert.DeserializeObject<KeyValuePair<string, string>>(dictAccounts[Constants.FIELD_TYPE]).Value : null;
                                string strAccountAddress = new JavaScriptSerializer().Serialize(dictAccounts);

                                var responseAccountAddress = JsonConvert.DeserializeObject<AddressAccountModel>(strAccountAddress);
                                responseAccountAddress.lastUpdatedDateTime = accModifiedOn;
                                responseAddress.addressTypeCode = Constants.FIELD_ADDRESSTYPECODE;
                                responseAddress.sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];
                                responseAccountAddress.addresses = new List<AddressModel>() { responseAddress };
                                responseAccountAddress.accountId = responseAccountAddress.srcPartyId;

                                responseAccountAddress.boClassCode = Constants.FIELD_BOCLASSCODE;

                                if (eventType == Constants.FIELD_ADDRESSSENDTOSAPEVENTTYPE)
                                {
                                    // Get Bill To & Ship To from agreement
                                    responseAccountAddress.eventType = eventType;  //SendTOSAP 
                                    string areegementId = dictContent.FirstOrDefault().Where(x => x.Key.Equals(Constants.FIELD_ID)).Select(x => x.Value).FirstOrDefault().ToString();
                                    responseAccountAddress = GetAddressByAgreementId(areegementId, responseAccountAddress);
                                }
                                else
                                {
                                    responseAccountAddress.eventType = !string.IsNullOrEmpty(ExternalId) ? Constants.FIELD_ADDRESSUPDATEEVENTTYPE : Constants.FIELD_ADDRESSCREATEEVENTTYPE;
                                    responseAccountAddress.addresses.FirstOrDefault().sendToSAPIndicator = !string.IsNullOrEmpty(ExternalId) ? 1 : 0;
                                }
                                var adrid = responseAccountAddress.addresses[0].sourceAddressId;
                                objResponse = new JavaScriptSerializer().Serialize(responseAccountAddress);
                                var add_AccResponse = HttpActions.PutRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, addressSyncUrl, objResponse, muleXSourceSystem, "v2/accounts/" + responseAccountAddress.accountId));
                                #endregion

                                if (add_AccResponse.IsSuccessStatusCode)
                                {
                                    #region UpsertAddressData
                                    List<Dictionary<string, object>> dictContentUpsertAddress = new List<Dictionary<string, object>>();

                                    var dict = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(add_AccResponse.Content.ReadAsStringAsync().Result);

                                    //var id = dict.ContainsKey(Constants.FIELD_MULERESPONSEID) ? dict[Constants.FIELD_MULERESPONSEID] : string.Empty;
                                    var id = dict.ContainsKey(Constants.FIELD_MDMID) ? dict[Constants.FIELD_MDMID] : string.Empty;
                                    var extId = dict.ContainsKey(Constants.FIELD_CONSUMERID) ? dict[Constants.FIELD_CONSUMERID] : string.Empty;

                                    //HardCoded ExtId For Testing
                                    //id = adrid;
                                    //extId = adrid;
                                    //if (responseAccountAddress.eventType == Constants.FIELD_ADDRESSUPDATEEVENTTYPE)
                                    //    id = adrid + "_Update";
                                    //else if (responseAccountAddress.eventType == Constants.FIELD_ADDRESSSENDTOSAPEVENTTYPE)
                                    //    id = adrid + "_SendToSAP";

                                    if (!string.IsNullOrEmpty(id) && (!string.IsNullOrEmpty(extId) || (!string.IsNullOrEmpty(adrid))))
                                    {
                                        if (!string.IsNullOrEmpty(extId))
                                        {
                                            dictContentUpsertAddress.Add(new Dictionary<string, object> { { Constants.FIELD_ID, extId }, { Constants.FIELD_EXTERNALID, id } });
                                        }
                                        else
                                        {
                                            dictContentUpsertAddress.Add(new Dictionary<string, object> { { Constants.FIELD_ID, adrid }, { Constants.FIELD_EXTERNALID, id } });
                                        }

                                        //event Type SEND_TO_SAP,Update SENDTOSAP FLAG set true
                                        if (eventType == Constants.FIELD_ADDRESSSENDTOSAPEVENTTYPE)
                                        {
                                            dictContentUpsertAddress.Add(new Dictionary<string, object> { { Constants.FIELD_SENDTOSAP, "true" } });
                                        }

                                        var response = Utilities.UpdateMuleResponse(dictContentUpsertAddress, Constants.OBJ_ACCOUNTLOCATION);

                                        return Utilities.CreateResponse(response);
                                    }
                                    else
                                    {
                                        return Utilities.CreateResponse(add_AccResponse);
                                    }

                                    #endregion
                                }
                                else
                                {
                                    return add_AccResponse;
                                }
                            }
                        }
                    }
                }
                return null;
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Use for Address validation
        /// </summary>
        /// <param name="requestContent"></param>
        /// <returns></returns>
        public HttpResponseMessage AddressValidator(AddressValidationModel responseAddr)
        {
            try
            {
                string addressValidatorApiName = ConfigurationManager.AppSettings[Constants.CONFIG_ADDRESSVALIDATORAPINAME];

                string muleAddressSearchUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MULEADDRESSSEARCHURL];
                string jsonString = new JavaScriptSerializer().Serialize(responseAddr);
                var responseMessage = HttpActions.PostRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, muleAddressSearchUrl, jsonString, muleXSourceSystem, addressValidatorApiName));

                return responseMessage;
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Use for Address Search
        /// </summary>
        /// <param name="requestContent"></param>
        /// <returns></returns>
        public HttpResponseMessage AddressSuggest(Dictionary<string, string> queryAddr)
        {
            try
            {
                string addressSearchApiName = ConfigurationManager.AppSettings[Constants.CONFIG_ADDRESSSEARCHAPINAME];

                var muleHaderModel = new MuleHeaderModel();
                muleHaderModel.AccessToken = Utilities.GetMuleAuthToken();
                muleHaderModel.XClientId = muleXClientId;
                muleHaderModel.AppUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];
                muleHaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                muleHaderModel.APIName = addressSearchApiName;

                string query = string.Empty;
                if (queryAddr != null)
                {
                    if (queryAddr.Count > 0)
                    {
                        foreach (var parm in queryAddr)
                            query += parm.Key + Constants.CHAR_EQUAL + parm.Value + Constants.CHAR_AMPERSAND;
                    }
                    muleHaderModel.APIName = muleHaderModel.APIName + Constants.CHAR_QUESTIONMARK + query;
                }
                var response = HttpActions.GetRequestMule(muleHaderModel);

                return response;
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }
        #endregion

        /// <summary>
        /// Get address by agreement id
        /// </summary>
        /// <param name="agreementId"></param>
        /// <param name="responseAccountAddress"></param>
        /// <returns></returns>
        public AddressAccountModel GetAddressByAgreementId(string agreementId, AddressAccountModel responseAccountAddress)
        {
            string responseString = string.Empty;

            var query = new Query(Constants.OBJ_AGREEMENT);
            query.AddColumns(Constants.OBJ_SENDTOSAP_ACCOUNTLOCATION.Split(Constants.CHAR_COMMA));
            Expression exp = new Expression(ExpressionOperator.AND);
            exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, agreementId));
            query.SetCriteria(exp);

            Join ShipToAddress = new Join(Constants.OBJ_AGREEMENT, Constants.OBJ_ACCOUNTLOCATION, Constants.FIELD_SHIPTOADDRESS, Constants.FIELD_ID, JoinType.LEFT);
            ShipToAddress.EntityAlias = Constants.FIELD_SHIPTOADDRESS;

            Expression IsShip = new Expression(ExpressionOperator.AND);
            IsShip.AddCondition(new Condition("ext_ShipToAddress.ext_SendToSAP", FilterOperator.Equal, false));
            ShipToAddress.JoinCriteria = IsShip;
            query.AddJoin(ShipToAddress);

            Join BillToLocation = new Join(Constants.OBJ_AGREEMENT, Constants.OBJ_ACCOUNTLOCATION, Constants.FIELD_BILLTOADDRESS, Constants.FIELD_ID, JoinType.LEFT);
            BillToLocation.EntityAlias = Constants.FIELD_BILLTOADDRESS;
            Expression IsBill = new Expression(ExpressionOperator.AND);
            IsBill.AddCondition(new Condition("ext_BillToLocation.ext_SendToSAP", FilterOperator.Equal, false));
            BillToLocation.JoinCriteria = IsBill;
            query.AddJoin(BillToLocation);

            var jsonQuery = query.Serialize();

            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = Utilities.GetAuthToken();
            reqConfig.searchType = SearchType.AQL;
            reqConfig.objectName = Constants.OBJ_AGREEMENT;

            var response = Utilities.Search(jsonQuery, reqConfig);
            List<BillToShipToModel> lstAddresses = new List<BillToShipToModel>();

            if (response != null && response.IsSuccessStatusCode)
            {
                lstAddresses = JsonConvert.DeserializeObject<List<BillToShipToModel>>(JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString());
            }

            responseAccountAddress.addresses = new List<AddressModel>();

            if (lstAddresses != null && lstAddresses.Count > 0)
            {
                if (lstAddresses[0].ext_BillToLocation.Id == lstAddresses[0].ext_ShipToAddress.Id && !string.IsNullOrEmpty(lstAddresses[0].ext_BillToLocation.Id) && !string.IsNullOrEmpty(lstAddresses[0].ext_ShipToAddress.Id))
                {
                    responseAccountAddress.addresses.AddRange(lstAddresses.Select(x => new AddressModel()
                    {
                        orderAccountName = x.ext_BillToLocation.orderAccountName,
                        addressLine1 = x.ext_BillToLocation.addressLine1,
                        addressLine2 = x.ext_BillToLocation.addressLine2,
                        city = x.ext_BillToLocation.City,
                        stateCode = x.ext_BillToLocation.State,
                        countryCode = x.ext_BillToLocation.Country,
                        postalCode = x.ext_BillToLocation.PostalCode,
                        lastUpdatedDateTime = x.ext_BillToLocation.lastUpdatedDateTime,
                        sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM],
                        addressTypeCode = Constants.FIELD_ADDRESSTYPECODE,
                        sourceAddressId = x.ext_BillToLocation.Id,
                        primaryBillTo = x.ext_BillToLocation.primaryBillTo,
                        primaryShipTo = x.ext_BillToLocation.primaryShipTo,
                        sendToSAPIndicator = 1,
                        useOriginalIndicator = x.ext_BillToLocation.useOriginalIndicator,
                        vatRegistrationNumber = x.ext_BillToLocation.vatRegistrationNumber
                    }));
                }
                else
                {
                    if (!string.IsNullOrEmpty(lstAddresses[0].ext_BillToLocation.Id))
                    {
                        responseAccountAddress.addresses.AddRange(lstAddresses.Select(x => new AddressModel()
                        {
                            orderAccountName = x.ext_BillToLocation.orderAccountName,
                            addressLine1 = x.ext_BillToLocation.addressLine1,
                            addressLine2 = x.ext_BillToLocation.addressLine2,
                            city = x.ext_BillToLocation.City,
                            stateCode = x.ext_BillToLocation.State,
                            countryCode = x.ext_BillToLocation.Country,
                            postalCode = x.ext_BillToLocation.PostalCode,
                            lastUpdatedDateTime = x.ext_BillToLocation.lastUpdatedDateTime,
                            sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM],
                            addressTypeCode = Constants.FIELD_ADDRESSTYPECODE,
                            sourceAddressId = x.ext_BillToLocation.Id,
                            primaryBillTo = x.ext_BillToLocation.primaryBillTo,
                            primaryShipTo = x.ext_BillToLocation.primaryShipTo,
                            sendToSAPIndicator = 1,
                            useOriginalIndicator = x.ext_BillToLocation.useOriginalIndicator,
                            vatRegistrationNumber = x.ext_BillToLocation.vatRegistrationNumber
                        }));
                    }
                    if (!string.IsNullOrEmpty(lstAddresses[0].ext_ShipToAddress.Id))
                    {
                        responseAccountAddress.addresses.AddRange(lstAddresses.Select(x => new AddressModel()
                        {
                            orderAccountName = x.ext_ShipToAddress.orderAccountName,
                            addressLine1 = x.ext_ShipToAddress.addressLine1,
                            addressLine2 = x.ext_ShipToAddress.addressLine2,
                            city = x.ext_ShipToAddress.City,
                            stateCode = x.ext_ShipToAddress.State,
                            countryCode = x.ext_ShipToAddress.Country,
                            postalCode = x.ext_ShipToAddress.PostalCode,
                            lastUpdatedDateTime = x.ext_ShipToAddress.lastUpdatedDateTime,
                            sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM],
                            addressTypeCode = Constants.FIELD_ADDRESSTYPECODE,
                            sourceAddressId = x.ext_ShipToAddress.Id,
                            primaryBillTo = x.ext_ShipToAddress.primaryBillTo,
                            primaryShipTo = x.ext_ShipToAddress.primaryShipTo,
                            sendToSAPIndicator = 1,
                            useOriginalIndicator = x.ext_ShipToAddress.useOriginalIndicator,
                            vatRegistrationNumber = x.ext_ShipToAddress.vatRegistrationNumber
                        }));
                    }
                }
            }

            return responseAccountAddress;
        }
    }
}
