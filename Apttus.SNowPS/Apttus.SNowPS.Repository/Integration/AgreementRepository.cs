﻿/****************************************************************************************
@Name: AgreementRepository.cs
@Author: Akash Kakadiya
@CreateDate: 18 Oct 2017
@Description: Agreement Related Computations
@UsedBy: This will be used by AgreementController.cs

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Repository.Integration
{
    public class AgreementRepository
    {
        #region Properties
        string addressSyncUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        private string muleXSourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];
        #endregion
        /// <summary>
        /// Get Agreement Detail
        /// </summary>
        /// <param name="AgreementId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public string GetAgreementDetail(string AgreementId, string accessToken)
        {
            var agreementJson = string.Empty;
            //Create AQL Query
            try
            {
                Query queryAgreement = new Query(Constants.OBJ_AGREEMENT);
                queryAgreement.AddColumns(Constants.OBJ_AGREEMENT_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, AgreementId));
                queryAgreement.SetCriteria(exp);
                Join joinagreementDocument = new Join(Constants.OBJ_AGREEMENT, Constants.OBJ_AGREEMENTDOCUMENT, Constants.FIELD_ID, Constants.FIELD_AGREEMENTID, JoinType.LEFT);
                joinagreementDocument.EntityAlias = Constants.OBJ_AGREEMENTDOCUMENT;
                queryAgreement.AddJoin(joinagreementDocument);
                var jsonQuery = queryAgreement.Serialize();

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_AGREEMENT;

                var response = Utilities.Search(jsonQuery, reqConfig);
                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var agreementData = JsonConvert.DeserializeObject<List<AgreementResponse>>(responseString);

                    //Get json request for Mule
                    if (agreementData != null && agreementData.Count > 0)
                        agreementJson = BindResponseAgreementModel(agreementData);
                }
                return agreementJson;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Bind Object for Agreement
        /// </summary>
        /// <param name="agreementData"></param>
        /// <returns></returns>
        public string BindResponseAgreementModel(List<AgreementResponse> agreementData)
        {
            //Agreement Header Detail
            AgreementModel objAgreement = agreementData.Select(x => new AgreementModel()
            {
                Id = x.Id,
                AccountMDMID = x.AccountMDMID,
                RecordTypeId = x.RecordTypeId != null ? x.RecordTypeId.Name : null,
                Category = x.Category != null ? x.Category.Key : string.Empty,
                ContractNumber = x.ContractNumber,
                SNAgreementNumber = x.SNAgreementNumber,
                CustomerContractNumber = x.CustomerContractReference,
                AgreementNumber = x.AgreementNumber,
                QuoteId = x.QuoteId != null ? x.QuoteId.Id : null,
                Name = x.Name,
                ParentAgreementId = x.ParentAgreementId != null ? x.ParentAgreementId.Id : null,
                AgreementStartDate = x.AgreementStartDate,
                AgreementEndDate = x.AgreementEndDate,
                Term = x.Term,
                Status = x.Status != null ? x.Status.Key : string.Empty,
                StatusCategory = x.StatusCategory != null ? x.StatusCategory.Key : string.Empty,
                BaseCurrency = x.BaseCurrency != null ? x.BaseCurrency.Key : string.Empty,
                partnerAccount = x.partnerAccount,
                ACV = x.ACV,
                USDACV = x.USDACV,
                TCV = x.TCV,
                USDTCV = x.USDTCV,
                AnnualListPrice = x.AnnualListPrice,
                USDAnnualListPrice = x.USDAnnualListPrice,
                CreatedOn = x.CreatedOn,
                CreatedById = x.CreatedById.ExternalId != null ? x.CreatedById.ExternalId : null,
                ActivatedDate = x.ActivatedDate,
                ActivatedById = x.ActivatedById.ExternalId != null ? x.ActivatedById.ExternalId : null,
                OpportunitySysId = x.OpportunitySysId,
                PartnershipType = x.PartnershipType != null ? x.PartnershipType.Key : string.Empty,
                TerritoryMDMID = x.TerritoryMDMID,
                QuoteOwner = x.QuoteOwner
            }).FirstOrDefault();

            if (objAgreement != null && objAgreement.Status.Equals(Constants.STR_AGREEMENTSTATUS))
            {

                var agreementDocument = agreementData.Where(x => x.Document != null && !string.IsNullOrEmpty(x.Document.Id)).Select(x => new Document()
                {
                    Id = x.Document.Id,
                    Name = x.Document.Name,
                    Type = x.Document.Type != null ? x.Document.Type.Key : null
                }).ToList();

                if (agreementDocument != null && agreementDocument.Count > 0)
                {
                    objAgreement.Documents = new List<Document>();
                    List<Document> objDistinct = agreementDocument.GroupBy(x => x.Id).Select(y => y.First()).ToList();
                    List<string> lstProductId = objDistinct.Select(x => x.Id).ToList();
                    objAgreement.Documents.AddRange(objDistinct);
                }
            }
            //Get json
            return JsonConvert.SerializeObject(objAgreement);
        }


        #region Upsert Agreement

        /// <summary>
        /// Use for Agreement upsert
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertAgreement(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                var respContacts = string.Empty;
                List<Model.ErrorInfo> errors = new List<Model.ErrorInfo>();
                var responseAgreement = new HttpResponseMessage();
                if (dictContent != null && dictContent.Count() > 0)
                {
                    List<Dictionary<string, object>> agreementNoteContent = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> dicAgreement = new List<Dictionary<string, object>>();
                    dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                    //Set Default Status Category AND Status 
                    Parallel.ForEach(dictContent, agreement =>
                    {
                        if (agreement.ContainsKey(Constants.FIELD_RECORDTYPEID))
                        {
                            if (!agreement.ContainsKey(Constants.FIELD_STATUSCATEGORY))
                            {
                                agreement.Add(Constants.FIELD_STATUSCATEGORY, Constants.FIELD_STATUSCATEGORYREQUEST);
                            }
                            if (!agreement.ContainsKey(Constants.FIELD_STATUS))
                            {
                                agreement.Add(Constants.FIELD_STATUS, Constants.FIELD_STATUSREQUEST);
                            }
                        }
                    });

                    //Seperate Note Dictionary from Agreement Dictiponary
                    agreementNoteContent = GetAgreementNoteContent(dictContent);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                    //Resolve MDM Account Id with Apttus Account Id
                    reqConfig.objectName = Constants.OBJ_ACCOUNT;
                    reqConfig.externalFilterField = Constants.FIELD_AGREEMENTACCOUNTMDMID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_ACCOUNTID;
                    Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                   

                    //Resolve Requested By with Apttus User Sys ID
                    reqConfig.objectName = Constants.OBJ_USER;
                    reqConfig.externalFilterField = Constants.FIELD_ORGINATORUSERMDMID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_USERID;
                    Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                    ////Resolve Territory with Apttus Territory MDM Id
                    reqConfig.objectName = Constants.OBJ_TERRITORY;
                    reqConfig.externalFilterField = Constants.FIELD_STR_EXT_FIELDTERRITORY;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_EXT_FIELDTERRITORY;
                    Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                    //Resolve QuoteOwnerUser with Apttus User External ID
                    reqConfig.objectName = Constants.OBJ_USER;
                    reqConfig.externalFilterField = Constants.FIELD_QUOTEOWNERUSERMDMID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_QUOTEOWNERUSERID;
                    Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                    ////Resolve Partner with Apttus Account MDM Id
                    reqConfig.objectName = Constants.OBJ_ACCOUNT;
                    reqConfig.externalFilterField = Constants.FIELD_PARTNERACCOUNTMDMID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_PARTNERACCOUNTID;
                    Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                    ////Resolve Customer Account with Apttus Account MDM Id
                    reqConfig.objectName = Constants.OBJ_ACCOUNT;
                    reqConfig.externalFilterField = Constants.FIELD_CUSTOMERMDMID;
                    reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                    reqConfig.resolvedID = Constants.FIELD_CUSTOMERID;
                    Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                    ////Resolve Company entity with apttus Selling entity Id
                    ResolveCompanyEntity(dictContent, accessToken);

                    //Remove Unresolved request by externalID
                    Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_EXTERNALID);

                    //Resolve Agreement Type  with Apttus RecordTypeId of Agreement ID
                    reqConfig.objectName = Constants.OBJ_RECORDTYPE;
                    reqConfig.externalFilterField = Constants.FIELD_RECORDTYPEID;
                    reqConfig.apttusFilterField = Constants.FIELD_RECORDTYPENAME;
                    reqConfig.resolvedID = Constants.FIELD_RECORDTYPEID;
                    Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                    //Remove Unresolved request by Name
                    Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_RECORDTYPENAME);

                    if (dictContent != null && dictContent.Count > 0)
                    {
                        //Upsert Agreement
                        reqConfig.objectName = Constants.OBJ_AGREEMENT;
                        reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                        responseAgreement = Utilities.Upsert(dictContent, reqConfig);

                        if (responseAgreement.IsSuccessStatusCode)
                        {
                            ////Update Created By with Apttus User ID     
                            //var createdByResponseMessage = UpsertCreatedBy(dictContent, responseMessage, reqConfig);

                            ////note create in cmn_notes
                            var lstNote = GetNoteRequest(agreementNoteContent, responseAgreement);
                            if (lstNote.Count > 0)
                            {
                                var noteReqConfig = new RequestConfigModel();
                                noteReqConfig.objectName = Constants.OBJ_NOTES;
                                noteReqConfig.accessToken = accessToken;
                                var noteResponse = Utilities.Create(lstNote, noteReqConfig);
                                var jsonNoteResponse = JsonConvert.DeserializeObject(noteResponse.Content.ReadAsStringAsync().Result);
                                if (noteResponse.IsSuccessStatusCode)
                                    return Utilities.CreateResponse(responseAgreement, errors);
                                else
                                    return Utilities.CreateResponse(noteResponse, errors);
                            }
                            else
                            {
                                return Utilities.CreateResponse(responseAgreement, errors);
                            }
                        }
                        else
                        {
                            //Create Response
                            return Utilities.CreateResponse(responseAgreement, errors);

                        }
                    }
                    else
                    {
                        var bulkResponseModel = new BulkOperationResponseModel();
                        var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                        responseAgreement.Content = Utilities.CreateJsonContent(jsonResponse);

                        //Create Response by adding custom errors
                        return Utilities.CreateResponse(responseAgreement, errors);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Note Dictionary from agreement Dictionary
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetAgreementNoteContent(List<Dictionary<string, object>> dictContent)
        {
            List<Dictionary<string, object>> agreementNoteContent = new List<Dictionary<string, object>>();
            foreach (Dictionary<string, object> agreement in dictContent)
            {
                if (agreement.ContainsKey(Constants.FEILD_AGREMENTNOTE))
                {
                    Dictionary<string, object> noteDict = new Dictionary<string, object>();
                    noteDict.Add(Constants.FIELD_EXTERNALID, Convert.ToString(agreement[Constants.FIELD_EXTERNALID]));
                    noteDict.Add(Constants.FEILD_AGREMENTNOTE, Convert.ToString(agreement[Constants.FEILD_AGREMENTNOTE]));
                    agreementNoteContent.Add(noteDict);
                    agreement.Remove(Constants.FEILD_AGREMENTNOTE);
                }
            }
            return agreementNoteContent;
        }

        /// <summary>
        /// Get list of notes 
        /// </summary>
        /// <param name="dicNote"></param>
        /// <param name="responseMessage"></param>
        /// <returns></returns>
        public List<NoteModel> GetNoteRequest(List<Dictionary<string, object>> dicNote, HttpResponseMessage responseMessage)
        {
            List<NoteModel> lstNote = new List<NoteModel>();
            List<UpsertResponseModel> lstAgreementResponse = new List<UpsertResponseModel>();

            var responseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(responseMessage.Content.ReadAsStringAsync().Result);
            if (responseObj.Updated != null || responseObj.Inserted != null)
            {
                //Add id and externalid in list note from updated record
                if (responseObj.Updated != null && responseObj.Updated.Count > 0)
                {
                    for (int i = 0; i < responseObj.Updated.Count; i++)
                    {
                        var lstUpdatedAgreement = responseObj.Updated[i];
                        var jsonUpdatedAgreement = JsonConvert.SerializeObject(lstUpdatedAgreement);
                        var objUpdate = JsonConvert.DeserializeObject<UpsertResponseModel>(jsonUpdatedAgreement);
                        lstAgreementResponse.Add(objUpdate);
                    }
                }
                //Add id and externalid in list note from inserted record
                if (responseObj.Inserted != null && responseObj.Inserted.Count > 0)
                {
                    for (int i = 0; i < responseObj.Inserted.Count; i++)
                    {
                        var lstInsertedAgreement = responseObj.Inserted[i];
                        var jsonInsertedAgreement = JsonConvert.SerializeObject(lstInsertedAgreement);
                        var objInsert = JsonConvert.DeserializeObject<UpsertResponseModel>(jsonInsertedAgreement);
                        lstAgreementResponse.Add(objInsert);
                    }
                }
                foreach (var item in dicNote)
                {
                    //Compare externalid in note dictionary and agreement upsert response
                    var IsExistDate = lstAgreementResponse.Select(x => x)
                            .Where(x => x.ExternalId.Equals(item.Select(y => item[Constants.FIELD_EXTERNALID]).FirstOrDefault()))
                            .FirstOrDefault();
                    //If note dictionary and agreement upsert response contain same externalid ,Add note in note list 
                    if (IsExistDate != null && !string.IsNullOrEmpty(IsExistDate.Id))
                    {
                        NoteModel objNoteModel = new NoteModel()
                        {
                            ContextObject = new ContextObject()
                            {
                                Id = IsExistDate.Id,
                                Type = Constants.OBJ_AGREEMENT
                            },
                            Name = "Agreement Note",
                            NoteText = item[Constants.FEILD_AGREMENTNOTE].ToString()
                        };
                        lstNote.Add(objNoteModel);
                    }
                }
            }
            return lstNote;
        }

        /// <summary>
        /// Resolve Company Entity with Account id from "entity" account type and name
        /// </summary>
        /// <param name="dictContent"></param>
        public void ResolveCompanyEntity(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            string accountID = string.Empty;
            if (dictContent.Any(data => data.ContainsKey(Constants.FIELD_SNENITITY)))
            {
                //Get selling entity name
                var snEntity = dictContent.Any(data => data.ContainsKey(Constants.FIELD_SNENITITY) && Convert.ToString(data[Constants.FIELD_SNENITITY]) != null) ?
                                dictContent.Select(x => Convert.ToString(x[Constants.FIELD_SNENITITY])).FirstOrDefault() : null;

                //Search accountid base on account type and account name 
                Query queryAgreement = new Query(Constants.OBJ_ACCOUNT);
                queryAgreement.AddColumns(Constants.FIELD_ID);
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_TYPE, FilterOperator.Equal, Constants.FIELD_ACCOUNTTYPEENTITY));
                exp.AddCondition(new Condition(Constants.FIELD_NAME, FilterOperator.Equal, snEntity));
                queryAgreement.SetCriteria(exp);
                var jsonQuery = queryAgreement.Serialize();

                RequestConfigModel obj = new RequestConfigModel();
                obj.objectName = Constants.OBJ_ACCOUNT;
                obj.searchType = SearchType.AQL;
                obj.accessToken = accessToken;
                obj.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];
                var accountJson = Utilities.Search(jsonQuery, obj);
                if (accountJson.IsSuccessStatusCode)
                {
                    var resAccountJson = JObject.Parse(accountJson.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES);
                    accountID = resAccountJson.Select(data => data[Constants.FIELD_ID]).FirstOrDefault().ToString();

                    //Assign account id to selling entity id
                    foreach (var content in dictContent)
                    {
                        if (content.ContainsKey(Constants.FIELD_SNENITITY))
                        {
                            var resolvedField = accountID;
                            if (resolvedField != null)
                                content[Constants.FIELD_SNENITITY] = resolvedField;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Upsert Created By in Agreement 
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="responseMessage"></param>
        /// <param name="reqConfig"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertCreatedBy(List<Dictionary<string, object>> dictContent, HttpResponseMessage responseMessage, RequestConfigModel reqConfig)
        {
            List<UpsertResponseModel> lstAgreementResponse = new List<UpsertResponseModel>();
            HttpResponseMessage createdResponseMessage = new HttpResponseMessage();
            List<Dictionary<string, object>> lstDicCreated = new List<Dictionary<string, object>>();

            var responseObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(responseMessage.Content.ReadAsStringAsync().Result);

            if (responseObj.Updated != null || responseObj.Inserted != null)
            {
                //Add id and externalid in list from updated record
                if (responseObj.Updated != null && responseObj.Updated.Count > 0)
                {
                    for (int i = 0; i < responseObj.Updated.Count; i++)
                    {
                        var lstUpdatedAgreement = responseObj.Updated[i];
                        var jsonUpdatedAgreement = JsonConvert.SerializeObject(lstUpdatedAgreement);
                        var objUpdate = JsonConvert.DeserializeObject<UpsertResponseModel>(jsonUpdatedAgreement);
                        lstAgreementResponse.Add(objUpdate);
                    }
                }
                //Add id and externalid in list from inserted record
                if (responseObj.Inserted != null && responseObj.Inserted.Count > 0)
                {
                    for (int i = 0; i < responseObj.Inserted.Count; i++)
                    {
                        var lstInsertedAgreement = responseObj.Inserted[i];
                        var jsonInsertedAgreement = JsonConvert.SerializeObject(lstInsertedAgreement);
                        var objInsert = JsonConvert.DeserializeObject<UpsertResponseModel>(jsonInsertedAgreement);
                        lstAgreementResponse.Add(objInsert);
                    }
                }
            }
            //Add id and externalid,CreatedBy in Dictionary content  from Agreement Response list and requestorId
            foreach (var agreementResponse in lstAgreementResponse)
            {
                Dictionary<string, object> dicCreated = new Dictionary<string, object>();
                var requestorId = dictContent.Where(x => x.ContainsKey(Constants.FIELD_EXTERNALID)
                                    && x[Constants.FIELD_EXTERNALID] != null
                                    && x[Constants.FIELD_EXTERNALID].Equals(agreementResponse.ExternalId)
                                    && x.ContainsKey(Constants.FIELD_USERID)
                                    && x[Constants.FIELD_USERID] != null).Select(x => Convert.ToString(x[Constants.FIELD_USERID])).FirstOrDefault();

                if (!string.IsNullOrEmpty(requestorId))
                {
                    dicCreated.Add(Constants.FIELD_CREATEDBYID, requestorId);
                    dicCreated.Add(Constants.FIELD_ID, agreementResponse.Id);
                    dicCreated.Add(Constants.FIELD_EXTERNALID, agreementResponse.ExternalId);
                    lstDicCreated.Add(dicCreated);
                }
            }
            if (lstDicCreated.Count > 0)
            {
                reqConfig.objectName = Constants.OBJ_AGREEMENT;
                reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                createdResponseMessage = Utilities.Upsert(lstDicCreated, reqConfig);
            }
            return createdResponseMessage;
        }
        #endregion

        #region Sync Agreement Address
        /// <summary>
        /// Use for Agreement address sync and update send to sap flag
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage SyncAgreementAddress(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                var objResponse = string.Empty;
                string eventType = string.Empty;

                string addressSyncUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                List<Model.ErrorInfo> errors = new List<Model.ErrorInfo>();
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var dictAdd = dictContent.FirstOrDefault().ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                    if (dictAdd.ContainsKey(Constants.FIELD_EVENTTYPE))
                    {
                        eventType = dictAdd[Constants.FIELD_EVENTTYPE];
                        dictContent.FirstOrDefault().Remove(Constants.FIELD_EVENTTYPE);
                    }
                    //var requestConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNTLOCATION, Constants.FIELD_ID, Constants.FIELD_ID);
                    //var resMessageAddress = Utilities.Search(dictContent, requestConfig);
                    // if (resMessageAddress.IsSuccessStatusCode)
                    //{
                    string accountId = string.Empty;
                    string ExternalId = string.Empty;
                    AddressModel responseAddress = new AddressModel();
                    var reqConfig = new RequestConfigModel();

                    reqConfig.accessToken = accessToken;
                    reqConfig.select = new string[] { Constants.FIELD_ACCOUNTID };
                    reqConfig.objectName = Constants.OBJ_AGREEMENT;
                    reqConfig.apttusFilterField = Constants.FIELD_ID;
                    reqConfig.externalFilterField = Constants.FIELD_ID;

                    var searchresponse = Utilities.Search(dictContent, reqConfig);

                    var agreementAcc = JObject.Parse(searchresponse.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>().FirstOrDefault();
                    if (agreementAcc.ContainsKey(Constants.FIELD_ACCOUNTID))
                    {
                        accountId = JsonConvert.DeserializeObject<LookUp>(Convert.ToString(agreementAcc[Constants.FIELD_ACCOUNTID])).Id;
                    }
                    if (!string.IsNullOrEmpty(accountId))
                    {
                        List<Dictionary<string, object>> contentAccount = new List<Dictionary<string, object>>();

                        //Get Account Header Detail for Account
                        var reConfig = new RequestConfigModel();
                        reConfig.accessToken = accessToken;
                        reConfig.select = new string[] { Constants.FIELD_ID, Constants.FIELD_NAME };
                        reConfig.externalFilterField = Constants.FIELD_ID;
                        reConfig.apttusFilterField = Constants.FIELD_ID;
                        reConfig.objectName = Constants.OBJ_ACCOUNT;

                        //Add AccountId in Dictionary
                        var dic = new Dictionary<string, object>();
                        dic.Add(Constants.FIELD_ID, accountId);
                        contentAccount.Add(dic);
                        contentAccount = Utilities.GetCaseIgnoreDictContent(contentAccount);

                        var resMessageAccount = Utilities.Search(contentAccount, reConfig);

                        if (resMessageAccount.IsSuccessStatusCode)
                        {
                            var dictAccounts = new Dictionary<string, string>();

                            var resAccountString = JObject.Parse(resMessageAccount.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>().FirstOrDefault();

                            if (resAccountString != null && resAccountString.Count > 0)
                            {
                                #region SendAddressDataToMule
                                string addressId = string.Empty, addressExternalId = string.Empty;
                                dictAccounts = resAccountString.ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                                string strAccountAddress = new JavaScriptSerializer().Serialize(dictAccounts);

                                var responseAccountAddress = JsonConvert.DeserializeObject<AddressAccountModel>(strAccountAddress);

                                responseAddress.addressTypeCode = Constants.FIELD_ADDRESSTYPECODE;
                                responseAddress.sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];

                                responseAccountAddress.accountId = responseAccountAddress.srcPartyId;
                                responseAccountAddress.boClassCode = Constants.FIELD_BOCLASSCODE;

                                if (eventType == Constants.FIELD_ADDRESSSENDTOSAPEVENTTYPE)
                                {
                                    // Get Bill To Address & Ship To Address from agreement
                                    responseAccountAddress.eventType = eventType;  //SendTOSAP                                   
                                    string areegementId = dictContent.FirstOrDefault().Where(x => x.Key.Equals(Constants.FIELD_ID)).Select(x => x.Value).FirstOrDefault().ToString(); //Get AgreementID
                                    responseAccountAddress = GetAddressByAgreementId(areegementId, responseAccountAddress);
                                    if (responseAccountAddress.addresses.Count > 0)
                                    {
                                        addressId = responseAccountAddress.addresses[0].sourceAddressId;
                                        addressExternalId = responseAccountAddress.addresses[0].ExternalId;
                                    }
                                }
                                objResponse = new JavaScriptSerializer().Serialize(responseAccountAddress);
                                var muleHeaderModel = Utilities.GetMuleHeaderDetail(muleXClientId, addressSyncUrl, objResponse, muleXSourceSystem, "v2/accounts/" + responseAccountAddress.accountId);
                                var add_AccResponse = HttpActions.PutRequestMule(muleHeaderModel);
                                #endregion

                                if (add_AccResponse.IsSuccessStatusCode)
                                {
                                    #region UpsertAddressData
                                    List<Dictionary<string, object>> dictContentUpsertAddress = new List<Dictionary<string, object>>();

                                    var dict = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(add_AccResponse.Content.ReadAsStringAsync().Result);
                                    var id = dict.ContainsKey(Constants.FIELD_MDMID) ? dict[Constants.FIELD_MDMID] : string.Empty;
                                    var extId = dict.ContainsKey(Constants.FIELD_CONSUMERID) ? dict[Constants.FIELD_CONSUMERID] : string.Empty;

                                    if (!string.IsNullOrEmpty(id) && (!string.IsNullOrEmpty(extId)) || (!string.IsNullOrEmpty(addressId)))
                                    {
                                        if (!string.IsNullOrEmpty(extId))
                                        {
                                            dictContentUpsertAddress.Add(new Dictionary<string, object> { { Constants.FIELD_ID, extId }, { Constants.FIELD_EXTERNALID, id } });
                                        }
                                        else
                                        {
                                            dictContentUpsertAddress.Add(new Dictionary<string, object> { { Constants.FIELD_ID, addressId }, { Constants.FIELD_EXTERNALID, id } });
                                        }

                                        //event Type SEND_TO_SAP,Update SENDTOSAP FLAG set true
                                        if (eventType == Constants.FIELD_ADDRESSSENDTOSAPEVENTTYPE)
                                        {
                                            dictContentUpsertAddress.Add(new Dictionary<string, object> { { Constants.FIELD_SENDTOSAP, "true" } });
                                        }

                                        var response = Utilities.UpdateMuleResponse(dictContentUpsertAddress, Constants.OBJ_ACCOUNTLOCATION);

                                        return Utilities.CreateResponse(response);
                                    }
                                    else
                                    {
                                        return Utilities.CreateResponse(add_AccResponse);
                                    }

                                    #endregion
                                }
                                else
                                {
                                    return add_AccResponse;
                                }
                            }
                        }
                    }
                    //}

                }
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Get address by agreement id
        /// </summary>
        /// <param name="agreementId"></param>
        /// <param name="responseAccountAddress"></param>
        /// <returns></returns>
        public AddressAccountModel GetAddressByAgreementId(string agreementId, AddressAccountModel responseAccountAddress)
        {
            string responseString = string.Empty;

            var query = new Query(Constants.OBJ_AGREEMENT);
            query.AddColumns(Constants.OBJ_SENDTOSAP_ACCOUNTLOCATION.Split(Constants.CHAR_COMMA));
            Expression exp = new Expression(ExpressionOperator.AND);
            exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, agreementId));
            query.SetCriteria(exp);

            Join ShipToAddress = new Join(Constants.OBJ_AGREEMENT, Constants.OBJ_ACCOUNTLOCATION, Constants.FIELD_SHIPTOADDRESS, Constants.FIELD_ID, JoinType.LEFT);
            ShipToAddress.EntityAlias = Constants.FIELD_SHIPTOADDRESS;

            Expression IsShip = new Expression(ExpressionOperator.AND);
            IsShip.AddCondition(new Condition("ext_ShipToAddress.ext_SendToSAP", FilterOperator.Equal, false));
            ShipToAddress.JoinCriteria = IsShip;
            query.AddJoin(ShipToAddress);

            Join BillToLocation = new Join(Constants.OBJ_AGREEMENT, Constants.OBJ_ACCOUNTLOCATION, Constants.FIELD_BILLTOADDRESS, Constants.FIELD_ID, JoinType.LEFT);
            BillToLocation.EntityAlias = Constants.FIELD_BILLTOADDRESS;

            Expression IsBill = new Expression(ExpressionOperator.AND);
            IsBill.AddCondition(new Condition("ext_BillToLocation.ext_SendToSAP", FilterOperator.Equal, false));
            BillToLocation.JoinCriteria = IsBill;
            query.AddJoin(BillToLocation);

            var jsonQuery = query.Serialize();

            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = Utilities.GetAuthToken();
            reqConfig.searchType = SearchType.AQL;
            reqConfig.objectName = Constants.OBJ_AGREEMENT;

            var response = Utilities.Search(jsonQuery, reqConfig);
            List<BillToShipToModel> lstAddresses = new List<BillToShipToModel>();

            if (response != null && response.IsSuccessStatusCode)
            {
                lstAddresses = JsonConvert.DeserializeObject<List<BillToShipToModel>>(JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString());
            }

            responseAccountAddress.addresses = new List<AddressModel>();

            if (lstAddresses != null && lstAddresses.Count > 0)
            {
                if (lstAddresses[0].ext_BillToLocation.Id == lstAddresses[0].ext_ShipToAddress.Id
                        && !string.IsNullOrEmpty(lstAddresses[0].ext_BillToLocation.Id)
                        && !string.IsNullOrEmpty(lstAddresses[0].ext_ShipToAddress.Id))
                {
                    responseAccountAddress.addresses.AddRange(lstAddresses.Select(x => new AddressModel()
                    {
                        orderAccountName = x.ext_BillToLocation.orderAccountName,
                        addressLine1 = x.ext_BillToLocation.addressLine1,
                        addressLine2 = x.ext_BillToLocation.addressLine2,
                        city = x.ext_BillToLocation.City,
                        stateCode = x.ext_BillToLocation.State,
                        countryCode = x.ext_BillToLocation.Country,
                        postalCode = x.ext_BillToLocation.PostalCode,
                        lastUpdatedDateTime = x.ext_BillToLocation.lastUpdatedDateTime,
                        sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM],
                        addressTypeCode = Constants.FIELD_ADDRESSTYPECODE,
                        sourceAddressId = x.ext_BillToLocation.Id,
                        primaryBillTo = x.ext_BillToLocation.primaryBillTo,
                        primaryShipTo = x.ext_BillToLocation.primaryShipTo,
                        sendToSAPIndicator = 1,
                        useOriginalIndicator = x.ext_BillToLocation.useOriginalIndicator,
                        vatRegistrationNumber = x.ext_BillToLocation.vatRegistrationNumber,
                        ExternalId = x.ext_BillToLocation.ExternalId
                    }));
                }
                else
                {
                    if (!string.IsNullOrEmpty(lstAddresses[0].ext_BillToLocation.Id))
                    {
                        responseAccountAddress.addresses.AddRange(lstAddresses.Select(x => new AddressModel()
                        {
                            orderAccountName = x.ext_BillToLocation.orderAccountName,
                            addressLine1 = x.ext_BillToLocation.addressLine1,
                            addressLine2 = x.ext_BillToLocation.addressLine2,
                            city = x.ext_BillToLocation.City,
                            stateCode = x.ext_BillToLocation.State,
                            countryCode = x.ext_BillToLocation.Country,
                            postalCode = x.ext_BillToLocation.PostalCode,
                            lastUpdatedDateTime = x.ext_BillToLocation.lastUpdatedDateTime,
                            sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM],
                            addressTypeCode = Constants.FIELD_ADDRESSTYPECODE,
                            sourceAddressId = x.ext_BillToLocation.Id,
                            primaryBillTo = x.ext_BillToLocation.primaryBillTo,
                            primaryShipTo = x.ext_BillToLocation.primaryShipTo,
                            sendToSAPIndicator = 1,
                            useOriginalIndicator = x.ext_BillToLocation.useOriginalIndicator,
                            vatRegistrationNumber = x.ext_BillToLocation.vatRegistrationNumber,
                            ExternalId = x.ext_BillToLocation.ExternalId
                        }));
                    }
                    if (!string.IsNullOrEmpty(lstAddresses[0].ext_ShipToAddress.Id))
                    {
                        responseAccountAddress.addresses.AddRange(lstAddresses.Select(x => new AddressModel()
                        {
                            orderAccountName = x.ext_ShipToAddress.orderAccountName,
                            addressLine1 = x.ext_ShipToAddress.addressLine1,
                            addressLine2 = x.ext_ShipToAddress.addressLine2,
                            city = x.ext_ShipToAddress.City,
                            stateCode = x.ext_ShipToAddress.State,
                            countryCode = x.ext_ShipToAddress.Country,
                            postalCode = x.ext_ShipToAddress.PostalCode,
                            lastUpdatedDateTime = x.ext_ShipToAddress.lastUpdatedDateTime,
                            sourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM],
                            addressTypeCode = Constants.FIELD_ADDRESSTYPECODE,
                            sourceAddressId = x.ext_ShipToAddress.Id,
                            primaryBillTo = x.ext_ShipToAddress.primaryBillTo,
                            primaryShipTo = x.ext_ShipToAddress.primaryShipTo,
                            sendToSAPIndicator = 1,
                            useOriginalIndicator = x.ext_ShipToAddress.useOriginalIndicator,
                            vatRegistrationNumber = x.ext_ShipToAddress.vatRegistrationNumber,
                            ExternalId = x.ext_ShipToAddress.ExternalId
                        }));
                    }
                }
            }

            return responseAccountAddress;
        }
        #endregion


        #region PS
        /// <summary>
        /// Use Quote to lookup on milestone object for all records associated to Quote 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage GetMilestonesByQuoteId(string quoteId, string accessToken)
        {

            try
            {
                //Build AQL Query to fetch Quote & PS Milestones
                var query = new Query(Constants.OBJ_QUOTE);
                query.AddColumns(Constants.OBJ_PSMILESTONES_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, quoteId));

                query.SetCriteria(exp);

                Join PSMilestones = new Join(Constants.OBJ_QUOTE, Constants.OBJ_EXT_MILESTONE, Constants.FIELD_ID, Constants.FIELD_EXT_QUOTEID, JoinType.LEFT, Constants.OBJ_EXT_MILESTONE);
                query.AddJoin(PSMilestones);

                var jsonQuery = query.Serialize();

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_QUOTE;
                
                var response = Utilities.Search(jsonQuery, reqConfig);

                return Utilities.CreateResponse(response);
            }
            catch(Exception ex)
            {
                return new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, System.Text.Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
            }
        }
        #endregion
    }
}
