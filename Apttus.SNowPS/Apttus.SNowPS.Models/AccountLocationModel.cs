﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
﻿/****************************************************************************************
@Name: AccountLocation.cs
@Author: Vijay Kiran
@CreateDate: 27 Oct 2017
@Description: All require fields of Apttus for Defaulting Account Location on Quote
@UsedBy: This will be used by DefaultAccountLocation

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
namespace Apttus.SNowPS.Model
{
    public class AccountLocationModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string State { get; set; }
        
        [JsonProperty(PropertyName = "AccountId")]
        public LookUp AccountId { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "ext_PrimaryShipTo")]
        public string ext_PrimaryShipTo { get; set; }

        [JsonProperty(PropertyName = "ext_PrimaryBillTo")]
        public string ext_PrimaryBillTo { get; set; }

    }

    #region AccountLocation SPA
    public class AccountLocationsModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "ext_AddressLine1")]
        public string addressLine1 { get; set; }

        [JsonProperty(PropertyName = "ext_AddressLine2")]
        public string addressLine2 { get; set; }

        [JsonProperty(PropertyName = "ext_City")]
        public string city { get; set; }

        [JsonProperty(PropertyName = "ext_State")]
        public string stateCode { get; set; }

        [JsonProperty(PropertyName = "ext_PostalCode")]
        public string postalCode { get; set; }

        [JsonProperty(PropertyName = "ext_Country")]
        public string countryCode { get; set; }

        public string sourceSystem { get; set; }

        [JsonProperty(PropertyName = "ExternalId")]
        public string mdmId { get; set; }
    }
    #endregion
}
