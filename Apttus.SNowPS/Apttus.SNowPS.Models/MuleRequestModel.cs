﻿/****************************************************************************************
@Name: MuleRequestModel.cs
@Author: Akash Kakadiya
@CreateDate: 27 Sep 2017
@Description: Mule Request Model
@UsedBy: This will be used for mule request data binding 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using System;
using System.Net.Http;

namespace Apttus.SNowPS.Model
{
    public class MuleRequestModel
    {
        public int totalRecords { get; set; }

        public string batchID { get; set; }

        public Array recordsList { get; set; }
    }
    public class MuleHeaderModel
    {
        public string AccessToken { get; set; }
        public string XClientId { get; set; }
        public string MuleUrl { get; set; }
        public string XSourceSystem { get; set; }
        public string SendType { get; set; }
        public string TargetSystem { get; set; }
        public string APIName { get; set; }
        public HttpContent HttpContentStr { get; set; }
        public string XBatchId { get; set; }
        public string AppUrl { get; set; }

    }
}
