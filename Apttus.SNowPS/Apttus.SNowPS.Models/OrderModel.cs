﻿/****************************************************************************************
@Name: Order.cs
@Author: Meera Kant
@CreateDate: 10 Oct 2017
@Description: Order related properties 
@UsedBy: This will be used as a entity for Order, OrderLineItems and Invoices

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Apttus.SNowPS.Model
{
    public class Order
    {
        [JsonProperty(PropertyName = "orderID")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "orderNumber")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "orderType")]
        public string ext_QuoteType { get; set; }

        [JsonProperty(PropertyName = "purchaseOrderNumber")]
        public string PONumber { get; set; }

        //[JsonProperty(PropertyName = "orderActivationDate")]
        //public DateTime? ActivatedDate { get; set; }

        [JsonProperty(PropertyName = "orderStartDate")]
        public DateTime? OrderStartDate { get; set; }

        [JsonProperty(PropertyName = "orderEndDate")]
        public DateTime? OrderEndDate { get; set; }

        [JsonProperty(PropertyName = "companyName")]
        public string ext_SellingEntityCode { get; set; }

        [JsonProperty(PropertyName = "opportunityNumber")]
        public string ext_OpportunityNumber { get; set; }

        [JsonProperty(PropertyName = "quoteCurrencyCode")]
        public string ext_BaseCurrency { get; set; }

        [JsonProperty(PropertyName = "orderNotes")]
        public string ext_OrderARNotes { get; set; }

        [JsonProperty(PropertyName = "orderActivationDate")]
        public DateTime? ext_AgreementSignedDate { get; set; }

        [JsonProperty(PropertyName = "dataCenterName")]
        public string ext_datacenter { get; set; }

        [JsonProperty(PropertyName = "channel")]
        public string ext_salestype { get; set; }

        [JsonProperty(PropertyName = "priceBook")]
        public string PriceListId { get; set; }

        [JsonProperty(PropertyName = "renewalACV")]
        public decimal? ext_TotalNewRenewalACV { get; set; }

        [JsonProperty(PropertyName = "netPrice")]
        public decimal? ext_NetPrice { get; set; }

        [JsonProperty(PropertyName = "totalACVInUSD")]
        public decimal? ext_USDTotalNetNewACV { get; set; }

        [JsonProperty(PropertyName = "isTaxExempt")]
        public bool? ext_taxexempt { get; set; }

        [JsonProperty(PropertyName = "netNewACV")]
        public decimal? ext_TotalNetNewACV { get; set; }

        [JsonProperty(PropertyName = "salesRepNumber")]
        public string ext_PrimarySalesRepID { get; set; }

        [JsonProperty(PropertyName = "t4C")]
        public bool? ext_T4C { get; set; }

        [JsonProperty(PropertyName = "significantPenalty")]
        public bool? ext_SignificantPenalty { get; set; }

        [JsonProperty(PropertyName = "rightToRefund")]
        public bool? ext_RighttoRefund { get; set; }

        [JsonProperty(PropertyName = "penaltyAmount")]
        public decimal? ext_PenaltyAmt { get; set; }

        [JsonProperty(PropertyName = "refundPercentage")]
        public decimal? ext_RefundPercentage { get; set; }

        [JsonProperty(PropertyName = "noticePeriod")]
        public int? ext_NoticePeriod { get; set; }

        [JsonProperty(PropertyName = "psTerritoryID")]
        public string ext_PSTerritoryXRefID { get; set; }

        [JsonProperty(PropertyName = "salesTerritoryID")]
        public string ext_SalesTerritoryXRefID { get; set; }

        [JsonProperty(PropertyName = "agreementDetails")]
        public AgreementDetails AD { get; set; }

        //[JsonProperty(PropertyName = "salesDetails")]
        //public SalesDetails SD { get; set; }

        [JsonProperty(PropertyName = "accountDetails")]
        public AccountDetails ACD { get; set; }

        [JsonProperty(PropertyName = "orderLineItems")]
        public List<OrderLineItem> OLI { get; set; }

        [JsonProperty(PropertyName = "invoiceSchedule")]
        public List<InvoiceSchedule> ISS { get; set; }
    }
    public class AgreementDetails
    {
        [JsonProperty(PropertyName = "referenceAgreementNumber")]
        public string ext_ReferenceContract { get; set; }

        [JsonProperty(PropertyName = "agreementSignedDate")]
        public DateTime? ext_AgreementActivatedDate { get; set; }

        [JsonProperty(PropertyName = "paymentTerms")]
        public string PaymentTermId { get; set; }

        [JsonProperty(PropertyName = "parentAgreementNumber")]
        public string ext_ApttusParentContractNo { get; set; }

        [JsonProperty(PropertyName = "agreementStartDate")]
        public DateTime? ext_AgreementStartDate { get; set; }

        [JsonProperty(PropertyName = "agreementType")]
        public string ext_ContractType { get; set; }
    }

    //public class SalesDetails
    //{

    //}

    public class AccountDetails
    {
        [JsonProperty(PropertyName = "soldToMDMID")]
        public string ext_SoldToMDMID { get; set; }

        [JsonProperty(PropertyName = "shipToMDMID")]
        public string ext_ShipToMDMID { get; set; }

        [JsonProperty(PropertyName = "billToMDMID")]
        public string ext_BillToMDMID { get; set; }

        [JsonProperty(PropertyName = "mdmID")]
        public string ext_AccountMDMID { get; set; }

        [JsonProperty(PropertyName = "shipToParentMDMID")]
        public string ext_AffiliatedToCustomer { get; set; }

        [JsonProperty(PropertyName = "billingKeyContactEmailID")]
        public string ext_BillingContact { get; set; }

        [JsonProperty(PropertyName = "partnershipType")]
        public string ext_partnershiptype { get; set; }
    }
    public class OrderLineItem
    {
        [JsonProperty(PropertyName = "orderLineItemID")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "oliName")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "lineNumber")]
        public int? LineNumber { get; set; }

        [JsonProperty(PropertyName = "parentBundleNumber")]
        public int? ParentBundleNumber { get; set; }

        [JsonProperty(PropertyName = "itemSequence")]
        public int? ItemSequence { get; set; }

        [JsonProperty(PropertyName = "primaryLineNumber")]
        public int? PrimaryLineNumber { get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public decimal? Quantity { get; set; }

        [JsonProperty(PropertyName = "unitOfMeasure")]
        public string Uom { get; set; }

        [JsonProperty(PropertyName = "netPrice")]
        public decimal? ListPrice { get; set; }

        [JsonProperty(PropertyName = "extendedNetPrice")]
        public decimal? ExtendedPrice { get; set; }

        [JsonProperty(PropertyName = "adjustmentPercentage")]
        public decimal? NetAdjustmentPercent { get; set; }

        [JsonProperty(PropertyName = "adjustmentAmount")]
        public decimal? AdjustmentAmount { get; set; }

        [JsonProperty(PropertyName = "sellingPrice")]
        public decimal? NetPrice { get; set; }

        [JsonProperty(PropertyName = "orderLineItemStartDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "orderLineItemEndDate")]
        public DateTime? EndDate { get; set; }

        [JsonProperty(PropertyName = "agreementTerm")]
        public int? Term { get; set; }

        [JsonProperty(PropertyName = "productCode")]
        public string ext_ProductCode { get; set; }

        [JsonProperty(PropertyName = "productDescription")]
        public string ext_ProductDescription { get; set; }

        [JsonProperty(PropertyName = "deploymentRecordNumber")]
        public string ext_Deployment { get; set; }

        [JsonProperty(PropertyName = "netNewACV")]
        public decimal? ext_NetNewACV { get; set; }

        [JsonProperty(PropertyName = "renewalACV")]
        public decimal? ext_RenewalLineACV { get; set; }

        [JsonProperty(PropertyName = "itemTCV")]
        public decimal? ext_ItemTCV { get; set; }

        [JsonProperty(PropertyName = "billingFrequency")]
        public string BillingFrequency { get; set; }

        [JsonProperty(PropertyName = "taskID")]
        public string ext_TaskID { get; set; }

        [JsonProperty(PropertyName = "dataCenterName")]
        public string ext_Datacenter { get; set; }
    }

    public class InvoiceSchedule
    {
        [JsonProperty(PropertyName = "invoiceScheduleID")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "productCategoryName")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "quoteNumber")]
        public string ext_Quote { get; set; }

        [JsonProperty(PropertyName = "grandTotal")]
        public decimal? ext_GrandTotal { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal? ext_Amount { get; set; }

        [JsonProperty(PropertyName = "estimatedTax")]
        public decimal? ext_EstimatedTax { get; set; }

        [JsonProperty(PropertyName = "invoiceDisplay")]
        public string ext_InvoiceDisplay { get; set; }

        [JsonProperty(PropertyName = "orderSequence")]
        public string ext_OrderSequence { get; set; }

        [JsonProperty(PropertyName = "invoiceType")]
        public string ext_InvoiceType { get; set; }

        [JsonProperty(PropertyName = "invoiceSchedule")]
        public string ext_InvoiceSchedule { get; set; }

        [JsonProperty(PropertyName = "invoiceDate")]
        public DateTime? ext_InvoiceDate { get; set; }

    }

    /***********************  Response Quote from Apptus *******************************/
    public class ResponseOrder
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ChoiceType ext_QuoteType { get; set; }
        public string ext_SoldToMDMID { get; set; }
        public string ext_ShipToMDMID { get; set; }
        public string ext_BillToMDMID { get; set; }
        public string ext_AccountMDMID { get; set; }
        public string ext_AffiliatedToCustomer { get; set; }
        public string PONumber { get; set; }
        //public DateTime? ActivatedDate { get; set; }
        public DateTime? OrderStartDate { get; set; }
        public DateTime? OrderEndDate { get; set; }
        public string ext_SellingEntityCode { get; set; }
        public string ext_ReferenceContract { get; set; }
        public string ext_OpportunityNumber { get; set; }
        public string ext_BaseCurrency { get; set; }
        public string ext_OrderARNotes { get; set; }
        public DateTime? ext_AgreementSignedDate { get; set; }
        public ChoiceType ext_datacenter { get; set; }
        public ChoiceType ext_salestype { get; set; }
        public DateTime? ext_AgreementStartDate { get; set; }
        public ChoiceType ext_partnershiptype { get; set; }
        public LookUp PriceListId { get; set; }
        public decimal? ext_TotalNewRenewalACV { get; set; }
        public decimal? ext_NetPrice { get; set; }
        public decimal? ext_USDTotalNetNewACV { get; set; }
        public DateTime? ext_AgreementActivatedDate { get; set; }
        public LookUp PaymentTermId { get; set; }
        public string ext_ApttusParentContractNo { get; set; }
        public bool? ext_taxexempt { get; set; }
        public string ext_ContractType { get; set; }
        public decimal? ext_TotalNetNewACV { get; set; }
        public string ext_PrimarySalesRepID { get; set; }
        public string ext_BillingContact { get; set; }
        public bool? ext_T4C { get; set; }
        public bool? ext_SignificantPenalty { get; set; }
        public bool? ext_RighttoRefund { get; set; }
        public decimal? ext_PenaltyAmt { get; set; }
        public decimal? ext_RefundPercentage { get; set; }
        public int? ext_NoticePeriod { get; set; }
        public string ext_PSTerritoryXRefID { get; set; }
        public string ext_SalesTerritoryXRefID { get; set; }
		public string OriginalOrderNumber { get; set; }
		public OLI OLI { get; set; }
        public ISS ISS { get; set; }

        public DateTime? ActivatedDate { get; set; }
        public string ext_SAPContractID { get; set; }

        public string ext_IntegrationError { get; set; }
    }

    public class OLI
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int? ParentBundleNumber { get; set; }
        public int? ItemSequence { get; set; }
        public int? LineNumber { get; set; }
        public int? PrimaryLineNumber { get; set; }
        public decimal? Quantity { get; set; }
        public ChoiceType Uom { get; set; }
        public decimal? ListPrice { get; set; }
        public decimal? ExtendedPrice { get; set; }
        public decimal? NetAdjustmentPercent { get; set; }
        public decimal? AdjustmentAmount { get; set; }
        public decimal? NetPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Term { get; set; }
        public string ext_ProductCode { get; set; }
        public string ext_ProductDescription { get; set; }
        public string ext_Deployment { get; set; }
        public decimal? ext_NetNewACV { get; set; }
        public decimal? ext_RenewalLineACV { get; set; }
        public decimal? ext_ItemTCV { get; set; }
        public ChoiceType BillingFrequency { get; set; }
        public string ext_TaskID { get; set; }
        public string ext_Datacenter { get; set; }
        public string ext_SAPContractLineID { get; set; }
        public DateTime? ext_ScheduledProvisionDate { get; set; }
       public bool? ext_HiIntegrationProcessed { get; set; }
		public LookUp ProductId { get; set; }
		public LookUp OptionId { get; set; }
		public string ext_Instancename { get; set; }
	}
	public class ISS
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public LookUp ext_Quote { get; set; }
        public decimal? ext_GrandTotal { get; set; }
        public decimal? ext_Amount { get; set; }
        public decimal? ext_EstimatedTax { get; set; }
        public string ext_InvoiceDisplay { get; set; }
        public string ext_OrderSequence { get; set; }
        public string ext_InvoiceType { get; set; }
        public string ext_InvoiceSchedule { get; set; }
        public DateTime? ext_InvoiceDate { get; set; }
    }
	public class HiPlugins
	{
		public string Id { get; set; }
		public string ext_PluginName { get; set; }
		public bool? ext_AutomaticallyOnOff { get; set; }
		public LookUp ext_LKP { get; set; }
		public string ext_PluginId { get; set; }
	}


	/*public class ChoiceType
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class LookUp
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }*/

}

