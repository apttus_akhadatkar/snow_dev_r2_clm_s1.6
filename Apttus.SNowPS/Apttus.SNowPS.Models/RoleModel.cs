﻿/*************************************************************
@Name: RoleModel.cs
@Author: Nilesh Keshwala    
@CreateDate: 07-Nov-2017
@Description: This class contains classes & properties for 'Role' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************/

using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    public class RoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

}
