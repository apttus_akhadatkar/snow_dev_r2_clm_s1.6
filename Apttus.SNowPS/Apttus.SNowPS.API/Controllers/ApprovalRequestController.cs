﻿/****************************************************************************************
@Name: AgreementClauseController.cs
@Author: Bharat Kumbhar
@CreateDate: 08 Nov 2017
@Description: API calls related to Approval Request Entity
@UsedBy: Apttus AIC CLM

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/


using Apttus.SNowPS.Common;
using Apttus.SNowPS.Repository.CLM;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    /// <summary>
    /// Agreement Clause Contoller
    /// </summary>
    [ServiceRequestActionFilter]
    [RoutePrefix("api/snowps/v1/approvalrequest")]
    public class ApprovalRequestController : BaseController
    {
        [HttpPost]
        [Route("CopyAgreementId")]
        public IHttpActionResult CopyAgreementId([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid id;
                if (body.ContainsKey(Constants.FIELD_ID) && Guid.TryParse(body[Constants.FIELD_ID].ToString(), out id))
                {
                    var agreementClauseRepository = new ApprovalRequestRepository(AccessToken);
                    agreementClauseRepository.CopyAgreementId(id);
                    return Ok();
                }
                else
                {
                    throw new ArgumentException(Constants.ERR_INVALID_ARGUMENT_VALUE, nameof(id));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    return InternalServerError(ex.InnerException);
                }
                return InternalServerError(ex);
            }
        }
    }
}
