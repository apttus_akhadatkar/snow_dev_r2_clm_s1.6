﻿/****************************************************************************************
@Name: DefaultAccountLocationController.cs
@Author: Vijay Kiran
@CreateDate: 27 Oct 2017
@Description: All require fields of Apttus AccountLocation for Defaulting Account Location 
@UsedBy: This will be used by API to default the ShipToLocation and BillToLocation on Quote

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Apttus.SNowPS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Repository;

namespace Apttus.SNowPS.API.Controllers
{
    public class DefaultAccountLocationController : ApiController
    {
        // GET: api/DefaultAccountLocationController/ddeb5a85-1ab4-e711-80c2-000d3a31f4b7
        public HttpResponseMessage Get(string id)
        {
            
            try
                {
                    HttpResponseMessage respMsg = new HttpResponseMessage();                   
                    var headers = this.Request.Headers;
                    var accessToken = headers.Contains("Authorization") ? headers.GetValues("Authorization").First() : null;
                    
                    //Generate authentication token
                    if (accessToken == null)
                        accessToken = Utilities.GetAuthToken();

                QuoteModel quote = QuoteDefaultRepository.GetQuote(id, accessToken);
                if (quote.ShipToLocation == null & quote.BillToLocation == null)
                {
                    List<AccountLocationModel> accountLocation = new List<AccountLocationModel>();
                    string billTotLocationId = "";
                    string shipTotLocationId = "";
                    if (quote.salestype == Constants.MISC_INDIRECT)
                    {
                        if (quote.SalesPartner != null)
                        {

                            // Get BillTo & ShipTo Account Location
                            accountLocation = QuoteDefaultRepository.GetAccount(Convert.ToString(quote.SalesPartner.Id), accessToken);

                            foreach (var x in accountLocation)
                            {
                                if (x.ext_PrimaryBillTo.ToLower() == Constants.MISC_TRUE)
                                    billTotLocationId = x.Id;

                                if (x.ext_PrimaryShipTo.ToLower() == Constants.MISC_TRUE)
                                    shipTotLocationId = x.Id;

                            }


                        }
                    }
                    else
                    {
                        if (quote.AccountId != null)
                        {

                            // Get BillTo & ShipTo Account Location
                            accountLocation = QuoteDefaultRepository.GetAccount(Convert.ToString(quote.AccountId.Id), accessToken);


                            foreach (var x in accountLocation)
                            {
                                if (x.ext_PrimaryBillTo.ToLower() == Constants.MISC_TRUE)
                                    billTotLocationId = x.Id;

                                if (x.ext_PrimaryShipTo.ToLower() == Constants.MISC_TRUE)
                                    shipTotLocationId = x.Id;

                            }

                        }
                    }
                    //Update Quote (Upsert)
                    respMsg = QuoteDefaultRepository.UpdateQuote(id, quote.AccountId.Id, shipTotLocationId, billTotLocationId, accessToken);
                    return respMsg;
                }
                else
                return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent("BillTo and ShipTo locations are not null") };

                //return respMsg;
                
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
