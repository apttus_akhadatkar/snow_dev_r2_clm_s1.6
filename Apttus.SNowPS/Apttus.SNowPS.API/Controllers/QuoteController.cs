﻿/****************************************************************************************
@Name: QuoteController.cs
@Author: Meera Kant
@CreateDate: 1 Sep 2017
@Description: Resolves ExternalIDs and Upserts Quotes using generic API 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: Kiran Satani
@ModifiedDate: 25 Sep 2017
@ChangeDescription: Get Quote,Quote Line & Milestone by QuoteId to Send Mule Soft API

@ModifiedBy: Asha
@ModifiedDate: 12 Oct 2017
@ChangeDescription: Get quoteline items and calculate quote header financial fields
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Respository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using INTEGRATION = Apttus.SNowPS.Model.Integration;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class QuoteController : ApiController
    {
        private readonly QuoteRepository _quote = new QuoteRepository();
        private readonly ProdQuoteRepository _prodQuote = new ProdQuoteRepository();


        string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        string muleMockUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
        string muleQuoteSyncApi = ConfigurationManager.AppSettings[Constants.CONFIG_QUOTESYNCAPI];

        /// <summary>
        /// Quote Advance Search
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            try
            {
                string jsonResponse = string.Empty;
                if (this.Request != null && this.Request.RequestUri != null && !string.IsNullOrEmpty(this.Request.RequestUri.Query))
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var encodedQuery = this.Request.RequestUri.Query;
                    var decodedQuery = Utilities.GetDecodedQuery(encodedQuery);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_QUOTE;

                    var response = Utilities.GetSearch(decodedQuery, reqConfig);

                    return Utilities.CreateResponse(response);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Get Quote,Quote Line & Milestone by QuoteId to Send Mule Soft API to Synch
        /// </summary>
        /// <param name="dicQuote"></param>
        /// <returns></returns>
        [ActionName("Sync")]
        [HttpPost]
        public HttpResponseMessage SyncToMule(Dictionary<string, object> dicContent)
        {
            try
            {
                HttpResponseMessage httpMuleResponseMessage = null;
                if (dicContent != null && dicContent.Count > 0)
                {
					//Generate authentication token
					var accessToken = Utilities.GetAuthToken();

					//Get case-insensitive 
					var dicQuote = Utilities.GetCaseIgnoreSingleDictContent(dicContent);

					string Id = dicQuote.ContainsKey(Constants.FIELD_ID) && dicQuote[Constants.FIELD_ID] != null ? Convert.ToString(dicQuote[Constants.FIELD_ID]) : null;

					//Get Product Json For Mule
					var quoteJsonForMule = _quote.GetQuoteDetail(Id, accessToken);

                    //Call Mule API
                    if (!string.IsNullOrEmpty(quoteJsonForMule))
                    {

                        MuleHeaderModel objMuleHeaderModel = Utilities.GetMuleHeaderDetail(muleXClientId, muleMockUrl, quoteJsonForMule, null, muleQuoteSyncApi);
                        objMuleHeaderModel.APIName = ConfigurationManager.AppSettings[Constants.CONFIG_QUOTESYNCAPI];
                        objMuleHeaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                        objMuleHeaderModel.XClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
                        objMuleHeaderModel.HttpContentStr = new StringContent(quoteJsonForMule, Encoding.UTF8, "application/json");
                        objMuleHeaderModel.AccessToken = Utilities.GetMuleAuthToken();

                        httpMuleResponseMessage = HttpActions.PostRequestMule(objMuleHeaderModel);
                        if (httpMuleResponseMessage.IsSuccessStatusCode)
                        {
                            var muleResponseDict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(httpMuleResponseMessage.Content.ReadAsStringAsync().Result);

                            if (muleResponseDict != null && muleResponseDict.Count > 0)
                            {
                                var ignoreCaseMuleResDict = Utilities.GetCaseIgnoreSingleDictContent(muleResponseDict);
								if (ignoreCaseMuleResDict.ContainsKey(Constants.FIELD_ID) && ignoreCaseMuleResDict.ContainsKey(Constants.FIELD_CONSUMERID))
								{
									var id = ignoreCaseMuleResDict[Constants.FIELD_ID];
									var externalId = ignoreCaseMuleResDict[Constants.FIELD_CONSUMERID];
									var response = Utilities.UpdateMuleResponse(Convert.ToString(id), Convert.ToString(externalId), Constants.OBJ_QUOTE);
									return Utilities.CreateResponse(response);
								}
								else
									return httpMuleResponseMessage;
							}
                            return httpMuleResponseMessage;
                        }
                        return httpMuleResponseMessage;
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, "Data Not Found In Apttus System");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Quote Id");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }


        /// <summary>
        /// Resolves external ids and upserts quotes using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage UpsertQuote([FromBody]List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        return _quote.UpsertQuote(dictContent, accessToken);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        #region Renewal Opprotunity
        /// <summary>
        /// Create renewal opportuinity from quote id
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("CreateOpportunity")]
        [HttpPost]
        public HttpResponseMessage CreateOpportunity([FromBody]List<Dictionary<string, object>> dictContent)
        {
            try
            {
                var respContacts = string.Empty;
                List<ErrorInfo> errors = new List<ErrorInfo>();
                if (dictContent != null && dictContent.Count() > 0)
                {
                    dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var opportunity = _quote.CreateRenewalOpportunityToMule(dictContent, accessToken);
                    if (opportunity.IsSuccessStatusCode)
                    {
                        var mule = opportunity.Content.ReadAsStringAsync().Result;
                        HttpResponseMessage response = UpdateRenewalOppQuote(dictContent, accessToken, opportunity);
                        return response;
                    }
                    else
                    {
                        return Utilities.CreateResponse(opportunity);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        /// <summary>
        /// Method Use for upserts quotes on the base of mule auto renewal opportunity creation response.
        /// </summary>
        /// <param name="muleResponse"></param>
        /// <returns></returns>
        public HttpResponseMessage UpdateRenewalOppQuote(List<Dictionary<string, object>> dicContent, string accessToken, HttpResponseMessage muleResponse)
        {
            List<Dictionary<string, object>> objContent = _quote.UpdateRenewalQuoteDetail(dicContent, accessToken, muleResponse);
            HttpResponseMessage responseMessage = UpsertQuote(objContent);
            return responseMessage;
        }
        #endregion


        #region Quote Header Fields Calculation
        /// <summary>
		/// Get calculated financial fields for Quote header 
		/// </summary>
		/// <param name="dictContent">pass quoted parameters</param>
		/// <returns></returns>
		[ActionName("QuoteHeader")]
        [HttpPost]
        public HttpResponseMessage QuoteHeaderFiledsCalculation([FromBody]Dictionary<string, object> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    List<Dictionary<string, object>> dictQuotes = new List<Dictionary<string, object>>();
                    dictQuotes.Add(dicQuote);

                    //var headers = this.Request.Headers;
                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictQuotes);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Generate authentication token
                    var accessToken = Utilities.GetAuthToken();

                    //Get quote header financial fields after calculation
                    var resDetails = _prodQuote.GetQuoteDetail(Id, accessToken);
                    return resDetails;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide QuoteId");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }


        [ActionName("UpdateQuoteLineItemDetails")]
        [HttpPost]
        public HttpResponseMessage QuoteLineItemFieldCalculation([FromBody]Dictionary<string, object> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    List<Dictionary<string, object>> dictQuotes = new List<Dictionary<string, object>>();
                    dictQuotes.Add(dicQuote);

                    //var headers = this.Request.Headers;
                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictQuotes);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Generate authentication token
                    var accessToken = Utilities.GetAuthToken();

                    //Get quote header and line item financial fields calculation
                    var resDetails = _prodQuote.UpdateQuoteLineItemFields(Id, accessToken);
                    return resDetails;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide QuoteId");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }
        #endregion

        #region "default quote details"
        /// <summary>
        /// Default quote details based on rule conditions
        /// </summary>
        /// <param name="dicQuote"></param>
        /// <returns></returns>
        [ActionName("DefaultQuote")]
        [HttpPost]
        public HttpResponseMessage DefaultQuoteDetails([FromBody]Dictionary<string, object> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    List<Dictionary<string, object>> dictQuotes = new List<Dictionary<string, object>>();
                    dictQuotes.Add(dicQuote);

                    //var headers = this.Request.Headers;
                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictQuotes);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Generate authentication token
                    var accessToken = Utilities.GetAuthToken();

                    //Validate and default Quote details
                    var resDetails = _prodQuote.DefaultQuoteDetails(Id, accessToken);
                    return resDetails;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide QuoteId");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }


        /// <summary>
        ///  Validate quote details 
        /// </summary>
        /// <param name="dicQuote"></param>
        /// <returns></returns>
        [ActionName("ValidateQuote")]
        [HttpPost]
        public HttpResponseMessage ValidateQuoteDetails([FromBody]Dictionary<string, object> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    List<Dictionary<string, object>> dictQuotes = new List<Dictionary<string, object>>();
                    dictQuotes.Add(dicQuote);

                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictQuotes);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());
                    bool setIsPrimary = true;

                    //Generate authentication token
                    var accessToken = Utilities.GetAuthToken();

                    //Validate Quote details
                    var resDetails = _prodQuote.ValidateQuoteDetails(Id, setIsPrimary, accessToken);
                    return resDetails;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide QuoteId");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        #endregion

        /// <summary>
        /// Method Use for upserts quotes on the base of mule response.
        /// </summary>
        /// <param name="muleResponse"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertMuleOpportunity(HttpResponseMessage muleResponse)
        {
            var newresponse = GetRequestDictionary();
            HttpResponseMessage responseMessage = UpsertQuote(newresponse);
            string responseJson = responseMessage.Content.ReadAsStringAsync().Result;
            return responseMessage;
        }
        public List<Dictionary<string, object>> GetRequestDictionary()
        {
            List<Dictionary<string, object>> objContent = new List<Dictionary<string, object>>();
            INTEGRATION.ResponseQuote responsquote = new INTEGRATION.ResponseQuote();
            responsquote.AccountMDMID = "";
            responsquote.ExpectedStartDate = null;
            responsquote.ExpectedEndDate = null;
            responsquote.Term = 0;
            responsquote.Type = null;
            responsquote.ContractType = null;
            responsquote.TCV = 0;
            responsquote.Id = "";
            responsquote.RenewalACV = 0;
            string s = JsonConvert.SerializeObject(responsquote);
            objContent.Add(JsonConvert.DeserializeObject<Dictionary<string, object>>(s));
            return objContent;
        }

        #region Approvals On Quote

        /// <summary>
        /// To calculate custom fields for Approvals
        /// </summary>
        /// <param name="dicQuote"></param>
        /// <returns></returns>  
        [HttpPost]
        public HttpResponseMessage ApprovalFieldsCalculation(Dictionary<string, object> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    var dictQuotes = new List<Dictionary<string, object>> { dicQuote };
                    var accessToken = Request.Headers.Authorization != null ? Convert.ToString(Request.Headers.Authorization) : Utilities.GetAuthToken(); //Generate authentication token
                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictQuotes);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Validate and default Quote details
                    var resDetails = _prodQuote.CalculateApprovalFields(Id, accessToken);
                    return resDetails;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide QuoteId");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        #endregion Approvals On Quote
    }
}
