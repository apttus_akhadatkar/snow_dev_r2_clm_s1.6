﻿/****************************************************************************************
@Name: ContactController.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Resolves ExternalIDs and Upserts contacts using generic API 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Respository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class ContactController : ApiController
    {
        private readonly ContactRepository _contact = new ContactRepository();
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];

        /// <summary>
        /// Contact Advance Search
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            try
            {
                if (this.Request != null && this.Request.RequestUri != null && !string.IsNullOrEmpty(this.Request.RequestUri.Query))
                {
                    //Get Access Token from Headers
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var encodedQuery = this.Request.RequestUri.Query;
                    var decodedQuery = Utilities.GetDecodedQuery(encodedQuery);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_CONTACT;

                    var response = Utilities.GetSearch(decodedQuery, reqConfig);

                    return Utilities.CreateResponse(response);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Resolves external ids and upserts contacts using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage UpsertContact([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        return _contact.UpsertContact(dictContent, accessToken);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Use for Contact Sync
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Sync")]
        [HttpPost]
        public HttpResponseMessage ContactSync([FromBody] Dictionary<string, object> dictContent)
        {
            try
            {
                List<Dictionary<string, object>> lstDictContent = new List<Dictionary<string, object>>();
                lstDictContent.Add(dictContent);

                var response = _contact.SendContactDataToMule(lstDictContent);

                return response;
            }
            catch(Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }

        }
    }
}
