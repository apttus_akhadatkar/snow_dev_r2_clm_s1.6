﻿/****************************************************************************************
@Name: CustomSyncController.cs
@Author: Varun Shah
@CreateDate: 26 Sep 2017
@Description: Resolves ExternalIDs and Upserts Customer Instances using generic API  
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Repository.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class CustomerController : ApiController
    {
        private readonly CustomerRepository _customerInsances = new CustomerRepository();

        /// <summary>
        /// Resolves external ids and upserts Customer using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage UpsertCustomer([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                var respContacts = string.Empty;
                List<ErrorInfo> errors = new List<ErrorInfo>();

                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        return _customerInsances.UpsertCustomerInstance(dictContent, accessToken);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }
    }
}
