﻿/****************************************************************************************
@Name: LineItemController.cs
@Author:Rahul Der
@CreateDate: 11 Oct 2017
@Description: Calculation on financial fileds of cart line items using API 
@UsedBy: 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Respository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class LineItemController : ApiController
    {
        private readonly LineItemRepository _lineItemRepo = new LineItemRepository();

        /// <summary>
        /// To get line item financial fields details
        /// </summary>
        /// <returns></returns>
        /// 
        //[ActionName("Get")]
        [ActionName("GetLineItemDetails")]
        [HttpPost]
        public HttpResponseMessage GetLineItemDetails([FromBody]Dictionary<string, object> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    List<Dictionary<string, object>> dictQuotes = new List<Dictionary<string, object>>();
                    dictQuotes.Add(dicQuote);

                    //var headers = this.Request.Headers;
                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictQuotes);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Generate authentication token
                    var accessToken = Utilities.GetAuthToken();

                    var resDetails = _lineItemRepo.GetLineItemsFinancialFields(Id, accessToken);
                    return resDetails;
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide ConfigurationId");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }


    }
}
