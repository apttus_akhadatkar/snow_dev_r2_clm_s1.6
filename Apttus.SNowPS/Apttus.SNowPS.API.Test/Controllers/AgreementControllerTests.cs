﻿/****************************************************************************************
@Name: AgreementControllerTests.cs
@Author: Bhavinkumar Mistry
@CreateDate: 12 Nov 2017
@Description: Utilty for Unit Test Project
@UsedBy: This will be used by AgreementController for unit test.

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.API.Controllers;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Repository.CLM;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Apttus.SNowPS.API.Test.Controllers
{
    [TestClass()]
    public class AgreementControllerTests
    {
        static AgreementController agreementController;
        static AgreementRepository agreementRespository;

        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            agreementController = new AgreementController();
            agreementRespository = AgreementRepository.Instance(Utilities.GetAuthToken());
        }

        [TestMethod()]
        public void CalculatedAgreementFieldsFromQLI()
        {
            agreementController.Request = new HttpRequestMessage();
            agreementController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            agreementController.Request.Headers.Add("Authorization", Utilities.GetAuthToken());
            Dictionary<string, object> jsonContent = new Dictionary<string, object>();
            jsonContent.Add("Id", "a9c77672-e3c5-e711-80c2-0004ffb16e95");
            var test = agreementController.CopyRelatedParticipantsFromQuote(jsonContent);
            if (test != null)
            {
                Assert.IsTrue(false, "Fail while asserting");
            }
        }


        [TestMethod()]
        public void UpdateAgreementFieldsFromQLI()
        {
            agreementController.Request = new HttpRequestMessage();
            agreementController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            agreementController.Request.Headers.Add("Authorization", Utilities.GetAuthToken());
            Dictionary<string, object> jsonContent = new Dictionary<string, object>();
            jsonContent.Add("Id", "a9c77672-e3c5-e711-80c2-0004ffb16e95");
            var test = agreementController.UpdateAgreementFieldsFromQLI(jsonContent);
            if (test != null)
            {
                Assert.IsTrue(false, "Fail while asserting");
            }
        }
    }
}
