﻿/*************************************************************
@Name: ConversionHelper.cs
@Author: Chirag Modi
@CreateDate: 19-Sep-2017
@Description: This class contains quick conversion helper extensions to convert values from one type to another.
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/
using System;

namespace Apttus.SNowPS.Common
{
    public static class ConversionHelper
    {
        /// <summary>
        /// To convert any object value to 'string' type value.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>string: possible string value else null</returns>
        public static string ConvertToString(this object value)
        {
            string returnValue;
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    returnValue = null;
                }
                else
                {
                    returnValue = Convert.ToString(value).Trim();
                }
            }
            catch
            {
                returnValue = null;
            }

            return returnValue;
        }

        /// <summary>
        /// To convert any object value to 'string' type value with null.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>string: possible string value else null</returns>
        public static string ConvertToStringNull(this object value)
        {
            string returnValue;
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    return null;
                }
                else
                {
                    returnValue = Convert.ToString(value).Trim();
                    return returnValue == string.Empty ? null : returnValue;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// To convert any object value to 'double' type value.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>double: possible double value else 0</returns>
        public static double ConvertToDouble(this object value)
        {
            double returnValue;
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    returnValue = 0;
                }
                else
                {
                    double.TryParse(Convert.ToString(value), out returnValue);
                }
            }
            catch
            {
                returnValue = 0;
            }

            return returnValue;
        }

        /// <summary>
        /// To convert any object value to 'decimal' type value.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>decimal: possible decimal value else 0</returns>
        public static decimal ConvertToDecimal(this object value)
        {
            decimal returnValue;
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    returnValue = 0;
                }
                else
                {
                    decimal.TryParse(Convert.ToString(value), out returnValue);
                }
            }
            catch
            {
                returnValue = 0;
            }

            return returnValue;
        }

        /// <summary>
        /// To convert any object value to 'int' type value.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>int: possible int value else 0</returns>
        public static int ConvertToInteger(this object value)
        {
            int returnValue;
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    returnValue = 0;
                }
                else
                {
                    int.TryParse(Convert.ToString(value), out returnValue);
                }
            }
            catch
            {
                returnValue = 0;
            }

            return returnValue;
        }

        /// <summary>
        /// To convert any object value to 'int' type value with null.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>int: possible int value else null</returns>
        public static int? ConvertToNullableInteger(this object value)
        {
            int? returnValue;
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    return null;
                }
                else
                {
                    var result = 0;
                    int.TryParse(Convert.ToString(value), out result);
                    returnValue = result;
                }
            }
            catch
            {
                return null;
            }

            return returnValue;
        }

        /// <summary>
        /// To convert any object value to 'DateTime' type value with null.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>DateTime: possible DateTime value else null</returns>
        public static DateTime? ConvertToDate(this object value)
        {
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    return null;
                }
                else
                {
                    DateTime returnValue;
                    return DateTime.TryParse(Convert.ToString(value), out returnValue) ? returnValue : (DateTime?)null;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// To convert any object value to 'bool' type value with null.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>bool: possible bool value else null</returns>
        public static bool? ConvertToBool(this object value)
        {
            bool? returnValue = false;
            try
            {
                if (value == DBNull.Value || value == null)
                {
                    return null;
                }
                else
                {
                    returnValue = Convert.ToBoolean(value);
                }
            }
            catch
            {
                returnValue = null;
            }

            return returnValue;
        }

        /// <summary>
        /// To convert any object value to 'T' type value.
        /// </summary>
        /// <param name="value">Any type of object value</param>
        /// <returns>T: possible 'T' value else null</returns>
        public static T ConvertToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
